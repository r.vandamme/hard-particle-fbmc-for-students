# FBMC
Floppy-box Monte Carlo (FBMC) is a simulation method for obtaining the crystal structure of atomic or colloidal particles.
This code implements the algorithm described in [this paper](https://dspace.library.uu.nl/handle/1874/259898) to convex hard particles.

## Building ##

Linux:

0. CMake version 2.8.12+ is required. To get CMake on Ubuntu, simply type `sudo apt install cmake`.
1. Compile the Voro++ libraries: go to [FBMC_DIR]/external/voro++-0.4.6/ and type `make install`.
1. Go to the main FBMC directory. You should see the folders 'src' and 'obj'. Go to the folder 'bin' or create it if it does not yet exist.
2. In FBMC/bin, run the commands `cmake ..` and `make`. Depending on which compiler / compiler version you have you may get warnings or errors. These should be harmless, but you can bug me about them if you wish.
3. If compilation was succesful, an executable named 'fbmc' should be in /FBMC/bin/. Now you can try the code!

## Usage ##

This code calculates the crystal structure of any convex(!) (sphero)polyhedron. To do this, it needs to know only two things: the shape to simulate, and how many particles per unit cell to try. To run a FBMC simulation, use `./wherever_you_placed_it/FBMC/bin/fbmc -N [#PARTICLES PER UNIT CELL] -s [.OBJ FILE PATH]`.

You can provide the shape you want to simulate as a .obj file. [Wavefront .obj](https://en.wikipedia.org/wiki/Wavefront_.obj_file) is a basic text file format for 3D mesh data. Many tools that can do something with 3D meshes (e.g. Mathematica) can output them in the .obj format. A few shapes are included in the 'obj' folder that you can use to test.

`-N [#PARTICLES PER UNIT CELL]` sets the number of particles per unit cell. For convex shapes, most dense packings tend to have unit cells with 1-4 particles. More centrosymmetric shapes tend to have fewer, less centrosymmetric more. The tetrahedron is very non-centrosymmetric, and [its densest known packing is a 4-particle dimer crystal](https://arxiv.org/abs/1001.0586). The most centrosymmetric shape, the sphere, has two densest packings, the 1-particle face-centered cubic (FCC) and the 2-particle hexagonal close packed (HCP) lattices.

There are two additional optional arguments you can provide: `-r [ROUNDING RATIO]` and `-c [MC CYCLES]`. The former takes the input mesh shape and interpolates between that "sharp" shape and a sphere. The rounding ratio is defined such that a ratio of 0 corresponds to the sharp shape, and a ratio of 1 to a sphere. The latter argument simply sets how many MC cycles the simulation will use to try and determine the crystal structure. The default is 10^7 cycles, which is usually enough and makes the simulation take roughly 1 to 10 minutes for 1-4 particle unit cells of most basic shapes on a decent CPU.

Credits/thanks to:
https://github.com/Grieverheart for a version of the GJK overlap algorithm.
