#include "main_fbmc.h"


InputParameters input_parameters;


/* --------- Process all command line input ---------
 * Sorts command line input into their relevant simulation components. Most notably, splits
 * simulation input from visualization input.
 */
void process_command_line_input(int argc, char *argv[]){
  printf("Input: '");
  for(int arg = 1; arg < argc; arg++){ printf("%s ",argv[arg]); }
  printf("'\n");

  // Check for not enough input
  if(argc < 2){
 		printf("Not enough arguments: please try ./fbmc -N [#PARTICLES] -s [.OBJ SHAPE FILE]\n");
 		exit(42);
 	}

  // Process command line input
  bool req_args[2] = {false};
 	if(argc >= 2){
 		for(int arg = 1; arg < argc; arg++){
 			if(strcmp(argv[arg],"-N") == 0){
        input_parameters.n_particles = strtol(argv[arg+1],nullptr,10);
        if(input_parameters.n_particles == 0){printf("Invalid number of particles.\n");exit(42);}
        printf("Argument '%s %s': set number of particles to %u.\n",argv[arg],argv[arg+1],input_parameters.n_particles);
 				arg+=1;
        req_args[0] = true;
 			}
 			else if(strcmp(argv[arg],"-s") == 0){
        input_parameters.shape_file_name = argv[arg+1];
        printf("Argument '%s %s': loading shape from file %s.\n",argv[arg],argv[arg+1],input_parameters.shape_file_name.c_str());
 				arg+=1;
        req_args[1] = true;
      }
      else if(strcmp(argv[arg],"-r") == 0){
        input_parameters.rounding_ratio = strtod(argv[arg+1],nullptr);
        printf("Argument '%s %s': Setting rounding ratio to %lf.\n",argv[arg],argv[arg+1],input_parameters.rounding_ratio);
 				arg+=1;
        if(input_parameters.rounding_ratio < 0.0 || input_parameters.rounding_ratio > 1.0){
          printf("Error: Rounding ratio %lf should be between 0 and 1.\n", input_parameters.rounding_ratio);
        }
      }
      else if(strcmp(argv[arg],"-c") == 0){
        input_parameters.total_cycles = strtol(argv[arg+1],nullptr,10);
        printf("Argument '%s %s': Setting number of MC cycles to %lu.\n",argv[arg],argv[arg+1],input_parameters.total_cycles);
 				arg+=1;
      }
			else{
        printf("Unknown input: '%s'. Please try './mc -N [#PARTICLES] -s [.OBJ SHAPE FILE]'.\n",argv[arg]);
        printf("Optional arguments are '-r [ROUNDING_RATIO]' and '-c [MC_CYCLES]'.\n");
				exit(42);
			}
 		}
 	}

  // Check that the minimum arguments have been set to run a valid simulation
  if( !(req_args[0] && req_args[1]) ){
    printf("Required arguments not set. Try: './mc -N [#PARTICLES] -s [.OBJ SHAPE FILE]'.\n");
    exit(42);
  }
}

/* --------- ======= MAIN ======= ---------
 *
 */
int main(int argc, char *argv[]){

	// Process command line input to get input parameters for simulation
	process_command_line_input(argc,argv);

	// Perform simulation
  Simulation sim(input_parameters);

	return 1; // Simulation ended successfully
}
