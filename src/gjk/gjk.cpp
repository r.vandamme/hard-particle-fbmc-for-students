// #include <eigen3/Eigen/Geometry>
#include "../../external/eigen3/Eigen/Geometry"
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <limits>
#include "gjk.h"
#include "shape/convex.h"

namespace overlap{
  namespace{
    inline double dot(const Eigen::Vector3d a, const Eigen::Vector3d b){
      return a.dot(b);
    }

    inline Eigen::Vector3d cross(const Eigen::Vector3d a, const Eigen::Vector3d b){
      return a.cross(b);
    }

    using uchar = unsigned char;
    using uint  = unsigned int;

    // Lookup table which tells us at which positions in the simplex array
    // to get our points a, b, c, d. I.e. if our bits are 0111 -> 7 -> {0, 1, 2}
    const uchar p_pos[16][3] = {
        {0, 0, 0}, {0, 0, 0}, {1, 0, 0}, {0, 1, 0},
        {2, 0, 0}, {0, 2, 0}, {1, 2, 0}, {0, 1, 2},
        {3, 0, 0}, {0, 3, 0}, {1, 3, 0}, {0, 1, 3},
        {2, 3, 0}, {0, 2, 3}, {1, 2, 3}, {0, 0, 0}
    };

    const uchar s_pos[] = {0, 0, 1, 0, 2, 0, 0, 0, 3}; //Lookup table for single enabled bit position
    //________________________^__^_____^___________^

		/*        inline double dot(const Eigen::Vector3d a, const Eigen::Vector3d b){
      return b.dot(a);
    }

    inline Eigen::Vector3d cross(const Eigen::Vector3d a, const Eigen::Vector3d b){
      return b.cross(a);
    }*/

    inline Eigen::Vector3d barycentric_coordinates(const Eigen::Vector3d& P, const Eigen::Vector3d& A, const Eigen::Vector3d& B, const Eigen::Vector3d& C){
      Eigen::Vector3d v0 = B - A, v1 = C - A, v2 = P - A;

      double d00 = dot(v0, v0);
      double d01 = dot(v0, v1);
      double d02 = dot(v0, v2);
      double d11 = dot(v1, v1);
      double d12 = dot(v1, v2);

      double w = (d00 * d12 - d01 * d02) / (d00 * d11 - d01 * d01);
      double v = (d00 >= d01)? (d02 - d01 * w) / d00: (d12 - d11 * w) / d01;
      //double v = (d00 >= d01)? d02 / d00 - (d01 / d00) * w: d12 / d01 - (d11 / d01) * w;
      double u = 1.0 - v - w;

      return Eigen::Vector3d(u, v, w);
    }

    class Simplex{
    public:
      Simplex(void):
        bits_(0), last_sb_(0), size_(0), max_vert2(0.0)
      {}

      uchar size(void)const{
        return size_;
      }

      void add_point(const Eigen::Vector3d& point){
        uchar b = ~bits_; //Flip bits
        b &= -b; //Last set (available) bit
        uchar pos = s_pos[b]; //Get the bit position from the lookup table
        last_sb_ = pos;
        bits_ |= b; //Insert the new bit
        ++size_;
        p_[pos] = point;
        double l2 = point.squaredNorm();
        if(l2 > max_vert2) max_vert2 = l2;
      }

      void add_point(const Eigen::Vector3d& point, const Eigen::Vector3d& pa, const Eigen::Vector3d& pb){
        add_point(point);
        a_[last_sb_] = pa;
        b_[last_sb_] = pb;
      }

      void remove_point(int p){
        bits_ ^= (1 << p); //Erase the bit at position p
        --size_;
      }

      const Eigen::Vector3d& get_last_point(void)const{
        return p_[last_sb_];
      }

      bool contains(const Eigen::Vector3d& point){
        uchar bits = bits_;
        for(int i = 0; i < 4; ++i, bits >>= 1){
            if((bits & 1) && (p_[i] == point)) return true;
        }
        return false;
        //const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];
        //for(int i = 0; i < size_ - 1; ++i){
        //    if(p_[pos[i]] == point) return true;
        //}
        //if(p_[last_sb_] == point) return true;
        //return false;
      }

      double max_vertex(void)const{
          return max_vert2;
      }

    	void translate(const Eigen::Vector3d& dr){
        //for(int k = 0; k < 4; ++k) p_[k] += dr;
        max_vert2 = 0.0;
        uchar bits = bits_;
        for(int i = 0; i < 4; ++i, bits >>= 1){
          if(bits & 1){
            p_[i] += dr;
            if(p_[i].squaredNorm() > max_vert2) max_vert2 = p_[i].squaredNorm();
          }
        }
      }


      void compute_closest_points(const Eigen::Vector3d& P, Eigen::Vector3d& pa, Eigen::Vector3d& pb){
        switch(size_){
        //IMPORTANT: We are having accuracy problems with this projection.
        case 3:{
          const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];
          const Eigen::Vector3d& aA = a_[last_sb_];
          const Eigen::Vector3d& aB = a_[pos[0]];
          const Eigen::Vector3d& aC = a_[pos[1]];
          const Eigen::Vector3d& bA = b_[last_sb_];
          const Eigen::Vector3d& bB = b_[pos[0]];
          const Eigen::Vector3d& bC = b_[pos[1]];

          const Eigen::Vector3d& A = p_[last_sb_];
          const Eigen::Vector3d& B = p_[pos[0]];
          const Eigen::Vector3d& C = p_[pos[1]];

          auto bary = barycentric_coordinates(P, A, B, C);

          pa = aA * bary[0] + aB * bary[1] + aC * bary[2];
          pb = bA * bary[0] + bB * bary[1] + bC * bary[2];

          //auto omg = (pa - pb);
          //omg /= omg.length();
          //auto shit = omg - P / P.length();
          //printf("%e\n", P.length());
          //printf("____ %e, %e, %e", P[0] / P.length(), P[1] / P.length(), P[2] / P.length());
          //printf("____ %e, %e, %e\n", omg[0], omg[1], omg[2]);
          //assert(fabs(shit[0]) < 1.0e-12);
          //assert(fabs(shit[1]) < 1.0e-12);
          //assert(fabs(shit[2]) < 1.0e-12);

          break;
        }
        case 2:{
          const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];
          const Eigen::Vector3d& aA = a_[last_sb_];
          const Eigen::Vector3d& aB = a_[pos[0]];
          const Eigen::Vector3d& bA = b_[last_sb_];
          const Eigen::Vector3d& bB = b_[pos[0]];
          double u, v;
          {
            const Eigen::Vector3d& A = p_[last_sb_];
            const Eigen::Vector3d& B = p_[pos[0]];

            auto AB = B - A;
            v = dot(AB, P - A) / AB.squaredNorm();
            u = 1.0 - v;
          }

          pa = aA * u + aB * v;
          pb = bA * u + bB * v;

          break;
        }
        case 1:{
          pa = a_[last_sb_];
          pb = b_[last_sb_];
          break;
        }
        default:
        break;
        }
      }


      void print(void)const{
        uchar bits = bits_;
        for(int i = 0; i < 4; ++i, bits >>= 1){
          if(bits & 1) printf("%d: %f, %f, %f\n", i, p_[i][0], p_[i][1], p_[i][2]);
        }
      }

      void closest(Eigen::Vector3d& dir);
      bool contains_origin(Eigen::Vector3d& dir);

    private:
      uchar bits_;
      uchar last_sb_;
      uchar size_;
      Eigen::Vector3d p_[4]; //up to 4 points / 3-Simplex
      Eigen::Vector3d a_[4]; //up to 4 points / 3-Simplex
      Eigen::Vector3d b_[4]; //up to 4 points / 3-Simplex
      double max_vert2;
    };

    inline void Simplex::closest(Eigen::Vector3d& dir){
      ///////////////////////////////////////////////
      //  Check if the origin is contained in the  //
      //  Minkowski sum.                           //
      ///////////////////////////////////////////////
      switch(size_){
      case 4:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];
        const Eigen::Vector3d& c = p_[pos[1]];
        const Eigen::Vector3d& d = p_[pos[2]];

        Eigen::Vector3d ab = b - a;
        Eigen::Vector3d ac = c - a;
        Eigen::Vector3d ad = d - a;

        ////////////////////* Vertex Case *///////////////////

        double dot_aba = dot(ab, a);
        double dot_aca = dot(ac, a);
        double dot_ada = dot(ad, a);

        if(dot_aba >= 0.0 && dot_aca >= 0.0 && dot_ada >= 0.0){
          dir = a; //Take direction passing through origin
          remove_point(pos[0]);
          remove_point(pos[1]);
          remove_point(pos[2]);
          break;
        }

        ////////////////////* Edge Cases *///////////////////
        //printf("%f, %f, %f - %f, %f, %f\n",
        //    dir[0] / dir.length(), dir[1] / dir.length(), dir[2] / dir.length(),
        //    shit[0] / shit.length(), shit[1] / shit.length(), shit[2] / shit.length());

        /* ab Edge case */
        double dot_abb = dot(ab, b);
        double dot_abPerp1 = dot_aba * dot(ac, b) - dot_abb * dot_aca;
        double dot_abPerp2 = dot_aba * dot(ad, b) - dot_abb * dot_ada;
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces abc, abd
        if(dot_abPerp1 <= 0.0 && dot_abPerp2 <= 0.0 && dot_aba <= 0.0){
          dir = a + (dot_aba / (dot_aba - dot(ab, b))) * ab;
          remove_point(pos[1]);
          remove_point(pos[2]);
          break;
        }

        /* ac Edge case */
        double dot_acc = dot(ac, c);
        double dot_acPerp1 = dot_aca * dot(ad, c) - dot_acc * dot_ada;
        double dot_acPerp2 = dot_aca * dot(ab, c) - dot_acc * dot_aba;
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces abc, acd
        if(dot_acPerp1 <= 0.0 && dot_acPerp2 <= 0.0 && dot_aca <= 0.0){
          dir = a + (dot_aca / (dot_aca - dot(ac, c))) * ac;
          remove_point(pos[0]);
          remove_point(pos[2]);
          break;
        }

        /* ad Edge case */
        double dot_add = dot(ad, d);
        double dot_adPerp1 = dot_ada * dot(ab, d) - dot_add * dot_aba;
        double dot_adPerp2 = dot_ada * dot(ac, d) - dot_add * dot_aca;
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces acd, abd
        if(dot_adPerp1 <= 0.0 && dot_adPerp2 <= 0.0 && dot_ada <= 0.0){
          dir = a + (dot_ada / (dot_ada - dot(ad, d))) * ad;
          remove_point(pos[0]);
          remove_point(pos[1]);
          break;
        }

        ////////////////////* Face Cases *///////////////////

        /* On abc side */
        // The origin should be on abc's side and between the half-spaces defined by ac and ab (normal to abc)
        {
          Eigen::Vector3d abxac = cross(ab, ac);
          if(dot(ad, abxac) * dot(a, abxac) > 0.0 && dot_abPerp1 >= 0.0 && dot_acPerp2 >= 0.0){
            /* Remove point d */
            remove_point(pos[2]);
            dir = (dot(ad, abxac) > 0.0)? -abxac: abxac;
            dir *= dot(dir, a) / dir.squaredNorm();
            break;
          }
        }

        /* On abd side */
        // The origin should be on abd's side and between the half-spaces defined by ab and ad (normal to abd)
        {
          Eigen::Vector3d abxad = cross(ab, ad);
          if(dot(ac, abxad) * dot(a, abxad) > 0.0 && dot_abPerp2 >= 0.0 && dot_adPerp1 >= 0.0){
            /* Remove point c */
            remove_point(pos[1]);
            dir = (dot(ac, abxad) > 0.0)? -abxad: abxad;
            dir *= dot(dir, a) / dir.squaredNorm();
            break;
          }
        }

        /* On acd side */
        // The origin should be on acd's side and between the half-spaces defined by ac and ad (normal to acd)
        {
          Eigen::Vector3d acxad = cross(ac, ad);
          if(dot(ab, acxad) * dot(a, acxad) > 0.0 && dot_acPerp1 >= 0.0 && dot_adPerp2 >= 0.0){
            /* Remove point b */
            remove_point(pos[0]);
            dir = (dot(ab, acxad) > 0.0)? -acxad: acxad;
            dir *= dot(dir, a) / dir.squaredNorm();
            break;
          }
        }

        /* On bcd side */
        // The origin should be on bcd's side
        {
          Eigen::Vector3d bcxbd = cross(c - b, d - b);
          if(dot(bcxbd, ab) * dot(bcxbd, b) < 0.0){
            /* Remove point a */
            remove_point(last_sb_);
            last_sb_ = pos[0];
            dir = (dot(ab, bcxbd) < 0.0)? -bcxbd: bcxbd;
            dir *= dot(dir, b) / dir.squaredNorm();
            break;
          }
        }

        /* 'else' should only be when the origin is inside the tetrahedron */
        break;
      }
      case 3:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];
        const Eigen::Vector3d& c = p_[pos[1]];

        Eigen::Vector3d ab = b - a;
        Eigen::Vector3d ac = c - a;

        // Check if O in vertex region A
        double dot_aba = -dot(ab, a);
        double dot_aca = -dot(ac, a);
        if(dot_aba <= 0.0 && dot_aca <= 0.0){
          dir = a; //Take direction passing through origin
          remove_point(pos[0]);
          remove_point(pos[1]);
          break;
        }

        // Check if O in edge region AB
        double dot_abb = -dot(ab, b);
        double dot_acb = -dot(ac, b);
        double vc = dot_aba * dot_acb - dot_abb * dot_aca;
        if(vc <= 0.0 && dot_aba >= 0.0 && dot_abb <= 0.0){
          dir = a + (dot_aba / (dot_aba - dot_abb)) * ab;
          /* Remove Point c */
          remove_point(pos[1]);
          break;
        }

        // Check if O in edge region AC
        double dot_abc = -dot(ab, c);
        double dot_acc = -dot(ac, c);
        double vb = dot_abc * dot_aca - dot_aba * dot_acc;
        if(vb <= 0.0 && dot_aca >= 0.0 && dot_acc <= 0.0){
          double w = dot_aca / (dot_aca - dot_acc);
          dir = a + w * ac;
          /* Remove Point b */
          remove_point(pos[0]);
          break;
      	}

        double va = dot_abb * dot_acc - dot_abc * dot_acb;
        dir = a + (ab * vb + ac * vc) / (va + vb + vc);
        break;
      }
      case 2:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];

        Eigen::Vector3d  ab = b - a;

        double t = -dot(ab, a);
        if(t <= 0.0){
          dir = a; //Take direction passing through origin
          remove_point(pos[0]);
          break;
        }

        double denom = ab.squaredNorm();
        if(t >= denom){
          remove_point(last_sb_);
          last_sb_ = pos[0];
          dir = b;
          break;
        }

        dir = a + ab * (t / denom);
        break;
      }
      case 1:
      {
        const Eigen::Vector3d& a = p_[last_sb_];
        dir = a;
        break;
      }
      default: break;
      }
    }


    inline bool Simplex::contains_origin(Eigen::Vector3d& dir){
      ///////////////////////////////////////////////
      //  Check if the origin is contained in the  //
      //  Minkowski sum.                           //
      ///////////////////////////////////////////////
      switch(size_){
      case 4:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];
        const Eigen::Vector3d& c = p_[pos[1]];
        const Eigen::Vector3d& d = p_[pos[2]];

        Eigen::Vector3d ab = b - a;
        Eigen::Vector3d ac = c - a;
        Eigen::Vector3d ad = d - a;

        ////////////////////* Face Cases *///////////////////

        /* On abc side */
        Eigen::Vector3d abxac = cross(ab, ac);
        bool abPerp1Pos = (dot(cross(abxac, ab), a) > 0.0);
        bool acPerp2Pos = (dot(cross(ac, abxac), a) > 0.0);
        // The origin should be on abc's side and between the half-spaces defined by ac and ab (normal to abc)
        {
          Eigen::Vector3d abcPerp = (dot(abxac, ad) > 0.0)? -abxac: abxac;
          if((dot(abcPerp, a) < 0.0) && !abPerp1Pos && !acPerp2Pos){
            /* Remove point d */
            remove_point(pos[2]);
            dir = abcPerp;
            break;
          }
        }

        /* On abd side */
        Eigen::Vector3d abxad = cross(ab, ad);
        bool abPerp2Pos = (dot(cross(abxad, ab), a) > 0.0);
        bool adPerp1Pos = (dot(cross(ad, abxad), a) > 0.0);
        // The origin should be on abd's side and between the half-spaces defined by ab and ad (normal to abd)
        {
          Eigen::Vector3d abdPerp = (dot(abxad, ac) > 0.0)? -abxad: abxad;
          if((dot(abdPerp, a) < 0.0) && !abPerp2Pos && !adPerp1Pos){
            /* Remove point c */
            remove_point(pos[1]);
            dir = abdPerp;
            break;
          }
        }

        /* On acd side */
        Eigen::Vector3d acxad = cross(ac, ad);
        bool acPerp1Pos = (dot(cross(acxad, ac), a) > 0.0);
        bool adPerp2Pos = (dot(cross(ad, acxad), a) > 0.0);
        // The origin should be on acd's side and between the half-spaces defined by ac and ad (normal to acd)
        {
          Eigen::Vector3d acdPerp = (dot(acxad, ab) > 0.0)? -acxad: acxad;
          if((dot(acdPerp, a) < 0.0) && !acPerp1Pos && !adPerp2Pos){
            /* Remove point b */
            remove_point(pos[0]);
            dir = acdPerp;
            break;
          }
        }

        ////////////////////* Edge Cases *///////////////////

        /* ab Edge case */
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces abc, abd
        if(abPerp1Pos && abPerp2Pos){
          dir = cross(cross(a, ab), ab);
          remove_point(pos[1]);
          remove_point(pos[2]);
          break;
        }

        /* ac Edge case */
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces abc, acd
        if(acPerp1Pos && acPerp2Pos){
          dir = cross(cross(a, ac), ac);
          remove_point(pos[0]);
          remove_point(pos[2]);
          break;
        }

        /* ad Edge case */
        // The origin must be inside the space defined by the intersection
        // of two half-space normal to the adjacent faces acd, abd
        if(adPerp1Pos && adPerp2Pos){
          dir = cross(cross(a, ad), ad);
          remove_point(pos[0]);
          remove_point(pos[1]);
          break;
        }

        /* 'else' should only be when the origin is inside the tetrahedron */
        return true;
      }
      case 3:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];
        const Eigen::Vector3d& c = p_[pos[1]];

        Eigen::Vector3d ab = b - a;
        Eigen::Vector3d ac = c - a;
        Eigen::Vector3d abxac = cross(ab, ac);

        ////////////////////* Edge Cases *///////////////////

        /* Origin on the outside of triangle and close to ab */
        if(dot(cross(ab, abxac), a) < 0.0){
          dir = cross(cross(a, ab), ab);
          /* Remove Point c */
          remove_point(pos[1]);
          break;
        }

        /* Origin on the outside of triangle and close to ac */
        if(dot(cross(abxac, ac), a) < 0.0){
          dir = cross(cross(a, ac), ac);
          /* Remove Point b */
          remove_point(pos[0]);
          break;
        }

        /////////////////////* Face Case *///////////////////
        dir = (dot(abxac, a) > 0.0)? -abxac: abxac;
        break;
      }
      case 2:
      {
        const uchar* pos = p_pos[(bits_ ^ (1 << last_sb_))];

        const Eigen::Vector3d& a = p_[last_sb_];
        const Eigen::Vector3d& b = p_[pos[0]];

        Eigen::Vector3d  ab = b - a;

        dir = cross(cross(a, ab), ab);
        break;
      }
      case 1:
      {
        const Eigen::Vector3d& a = p_[last_sb_];
        dir = -a;
        break;
      }
      default: break;
      }

      return false;
    }

  };//namespace

  bool gjk_boolean(
    const Transform& pa, const shape::Convex& a,
    const Transform& pb, const shape::Convex& b
  ){
    // printf("%f %f %f \n",pa.pos_[0],pa.pos_[1],pa.pos_[2]);
    // printf("%f %f %f \n",pb.pos_[0],pb.pos_[1],pb.pos_[2]);
    // Eigen::Vector3d cc(cross(Eigen::Vector3d(1,0,0),Eigen::Vector3d(0,1,0)));
    // printf("%f %f %f \n",cc[0],cc[1],cc[2]);

    Eigen::Vector3d dir = pb.pos - pa.pos;
    Simplex S;

    uint fail_safe = 0;

    Eigen::Matrix3d m_rot_a(pa.rot);
    Eigen::Matrix3d m_rot_b(pb.rot);
    Eigen::Matrix3d m_inv_rot_a(pa.rot.inverse());
    Eigen::Matrix3d m_inv_rot_b(pb.rot.inverse());

    do{
			// auto vertex_a = pa.pos_ + pa.rot_.rotate(pa.size_ * a.support(inv_rot_a.rotate(dir)));
			// auto vertex_b = pb.pos_ + pb.rot_.rotate(pb.size_ * b.support(inv_rot_b.rotate(-dir)));
      auto vertex_a = pa.pos + m_rot_a * (pa.size * a.support(m_inv_rot_a * dir));
      auto vertex_b = pb.pos + m_rot_b * (pb.size * b.support(m_inv_rot_b * -dir));
			// printf("%f %f %f \n",vertex_a[0],vertex_a[1],vertex_a[2]);
			// printf("%f %f %f \n",vertex_b[0],vertex_b[1],vertex_b[2]);
      //
			// Eigen::Vector3d temp_a = m_inv_rot_a * dir;
			// Eigen::Vector3d temp_b = m_inv_rot_b * -dir;
			// printf("1a: %f %f %f \n",temp_a[0],temp_a[1],temp_a[2]);
			// printf("1b: %f %f %f \n",temp_b[0],temp_b[1],temp_b[2]);
      //
			// temp_a = a.support(m_inv_rot_a * dir);
			// temp_b = b.support(m_inv_rot_b * -dir);
			// printf("  %f %f %f \n",temp_a[0],temp_a[1],temp_a[2]);
			// printf("  %f %f %f \n",temp_b[0],temp_b[1],temp_b[2]);
			// printf("i dir %d %f %f %f\n",fail_safe,dir[0],dir[1],dir[2]);

      auto new_point = vertex_a - vertex_b;
      double dn = dot(dir, new_point);
      if(dn < 0.0 || S.contains(new_point)) return false;
      S.add_point(new_point);
			// contains_origin also generates a new dir
      if(S.contains_origin(dir) || dir.squaredNorm() == 0.0) return true;
    }while(fail_safe++ < 100);

    printf("Encountered error in GJK boolean: Infinite Loop.\n Direction (%f, %f, %f)\n", dir[0], dir[1], dir[2]);

    return true;
  }

}
