#ifndef __TRANSFORM_H
#define __TRANSFORM_H

#include <iostream>

struct Transform{
    Eigen::Vector3d pos;
    Eigen::Quaterniond rot;
    double size;

		void print(void)const{
			printf(" %lf %lf %lf\n %lf %lf %lf %lf\n %lf \n",
				pos[0], pos[1], pos[2],
				rot.w(), rot.vec()[0], rot.vec()[1], rot.vec()[2],
				size
			);
		}
};

#endif
