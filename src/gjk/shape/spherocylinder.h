#ifndef SHAPE_SPHEROCYLINDER_H
#define SHAPE_SPHEROCYLINDER_H

#include "convex.h"

namespace shape{

  class Spherocylinder: public Convex{
	private:
		double diameter_;
		double aspect_ratio_;
		double length_;
		double volume_;
		double in_radius_;
		double out_radius_;
		// Since a Spherocylinder is a rounded shape with 2 vertices, we define it similar to a
		// SphereSwept Line. This should only be used for the support.
		shape::Convex *shape_;
		std::vector<Eigen::Vector3d> vertices_;
		std::vector<Eigen::Vector2i> edges_;
  public:
		// Only constuctors that specify the size are allowed
		explicit Spherocylinder(void) = delete;
		explicit Spherocylinder(char) = delete;
		explicit Spherocylinder(double diameter, double aspect_ratio) :
			diameter_(diameter), aspect_ratio_(aspect_ratio), length_(aspect_ratio_ * diameter_),
			volume_( M_PI/6.0*pow(diameter_,3) + 0.25*M_PI*diameter_*diameter_*(length_-diameter_) ),
			in_radius_(diameter_/2.0),
			out_radius_(length_/2.0)
		{
			// A spherocylinder is a line with thickness. So like SphereSwept, its support function is
			// that of a line plus some thickness. The length of the line is the total length of the
			// spherocylinder minus its diameter.
			shape_ = new shape::Line(length_-diameter_);
			// Vertices are the centers of the spheres at the ends
			vertices_.push_back( Eigen::Vector3d(-0.5 * (length_ - diameter_), 0.0, 0.0) );
			vertices_.push_back( Eigen::Vector3d(+0.5 * (length_ - diameter_), 0.0, 0.0) );
			edges_.push_back( Eigen::Vector2i(0,1) );
		};

		// We call the Line's support and add a thickness to get the right result
    Eigen::Vector3d support(const Eigen::Vector3d& dir)const{
      return shape_->support(dir) + (diameter_/2.0) * dir.normalized();
    }

		// getters
		double length(void)const      { return length_; }
		double aspect_ratio(void)const{ return aspect_ratio_; }
		double diameter(void)const    { return diameter_; }
		std::string name(void)const   { return "Spherocylinder"; }
		double volume(void)const      { return volume_; }
    double in_radius(void)const   { return in_radius_; }
    double out_radius(void)const  { return out_radius_; }


		// Comparison operator overloaders
		friend bool operator==(const shape::Spherocylinder& lhs, const shape::Spherocylinder& rhs){
			if(
				lhs.shape_        == rhs.shape_        && // Check whether Line-s are also equal
				lhs.diameter_     == rhs.diameter_     &&
				lhs.length_       == rhs.length_       &&
				lhs.aspect_ratio_ == rhs.aspect_ratio_ &&
				lhs.volume_       == rhs.volume_       &&
				lhs.in_radius_    == rhs.in_radius_    &&
				lhs.out_radius_   == rhs.out_radius_   &&
				lhs.vertices_     == rhs.vertices_     &&
				lhs.edges_        == rhs.edges_
			){return true;}else{return false;}
		};
		friend bool operator!=(const shape::Spherocylinder& lhs, const shape::Spherocylinder& rhs){
			return !(lhs == rhs);
		};



		// Getter functions for the vertices, edges and faces (spherocylinder has no faces)
		const std::vector<Eigen::Vector3d>& v(void)const{ return vertices_; }
		const std::vector<Eigen::Vector2i>& e(void)const{ return edges_;    }
		const std::vector<Eigen::Vector3i>& f(void)const{
			printf("Error: Requested face of shape::Spherocylinder class, which has no faces. Exiting!\n");
			exit(42);
			static std::vector<Eigen::Vector3i> empty_vec;
			return empty_vec;
		}


		// Save the shape as a mesh to an .obj file for visualization.
		// This could be optimized and simplified quite a bit.
		std::string save_to_obj(int nr)const{
			double q_cyl = 18; // How round our cylinders are. Higher is rounder.
			unsigned int q_sph = 18; // Roundness of spheres. Even number. Higher=rounder. Don't set too high!
			double radius = diameter_/2.0;

			Eigen::Vector3d vertex;
			Eigen::Vector3i face;

			// ====================================== CYLINDER ======================================
			// Make vertices and faces of a cylinder in z of height 0 (we set the height later)
			std::vector<Eigen::Vector3d> cyl_v;
			std::vector<Eigen::Vector3i> cyl_f;
			// First two vertices added manually: one vertex for the bottom, one for the top.
			vertex << radius, 0.0, 0.0;
			cyl_v.push_back(vertex);
			cyl_v.push_back(vertex);
			// Now the rest, and make the faces too
			for(double phi = (2.0*M_PI / (double)q_cyl); phi < 2.0*M_PI; phi += (2.0*M_PI / (double)q_cyl) ){
				vertex << radius*cos(phi), radius*sin(phi), 0.0;
				// One vertex for the bottom, one for the top
				cyl_v.push_back(vertex);
				cyl_v.push_back(vertex);
				// Make the new faces (vertices are in clockwise order)
				// face << cyl_v.size()-4, cyl_v.size()-3, cyl_v.size()-2;
				// cyl_f.push_back(face);
				// face << cyl_v.size()-1, cyl_v.size()-2, cyl_v.size()-3;
				// cyl_f.push_back(face);
				// Make the new faces (vertices are in anticlockwise order)
				face << cyl_v.size()-2, cyl_v.size()-3, cyl_v.size()-4;
				cyl_f.push_back(face);
				face << cyl_v.size()-3, cyl_v.size()-2, cyl_v.size()-1;
				cyl_f.push_back(face);
			}
			// Connect the last faces
			face << 0, cyl_v.size()-1, cyl_v.size()-2;
			cyl_f.push_back(face);
			face << cyl_v.size()-1, 0, 1;
			cyl_f.push_back(face);

			// ====================================== SPHERE ======================================
			// Make vertices and faces of the sphere by discretizing the angles
			std::vector<Eigen::Vector3d> sph_v;
			std::vector<Eigen::Vector3i> sph_f;
			// Top pole manually to prevent degeneracy
			sph_v.push_back( Eigen::Vector3d(0.0, 0.0, radius) );
			// Then the rest
			for(double theta = (M_PI / (double)q_sph); theta < M_PI; theta += (M_PI / (double)q_sph) ){
				for(double phi = 0; phi < 2.0*M_PI; phi += (2.0*M_PI / (double)q_sph) ){
					// Add a vertex of the sphere
					vertex << radius*sin(theta)*cos(phi), radius*sin(theta)*sin(phi), radius*cos(theta);
					sph_v.push_back(vertex);
				}
			}
			// Bottom pole manually to prevent degeneracy
			sph_v.push_back( Eigen::Vector3d(0.0, 0.0, -radius) );
			// Face indices, top pole first
			for(size_t j = 1; j < q_sph; j++){
				face << 0, j, j+1;
				sph_f.push_back(face);
			}
			face << 0, q_sph, 1;
			sph_f.push_back(face);
			// Then bulk
			for(size_t i = 1; i < q_sph-1; i++){
				for(size_t j = 0; j < q_sph; j++){ // 1+(i-1)*q_sph is because pole is 1 vertex
					face << 1+(i-1)*q_sph + j, 1+i*q_sph + j, 1+i*q_sph + j+1;
					sph_f.push_back(face);
					face << 1+(i-1)*q_sph + j, 1+i*q_sph + j+1, 1+(i-1)*q_sph + j+1;
					sph_f.push_back(face);
				}
				// Connect first vertices to last vertices in phi direction
				face << 1+(i-1)*q_sph + q_sph-1, 1+i*q_sph + q_sph-1, 1+i*q_sph + 0;
				sph_f.push_back(face);
				face << 1+(i-1)*q_sph + q_sph-1, 1+i*q_sph + 0, 1+(i-1)*q_sph + 0;
				sph_f.push_back(face);
			}
			// Then the bottom pole
			for(size_t j = sph_v.size()-1; j > sph_v.size() - q_sph; j--){
				face << sph_v.size()-1, j-1, j-2;
				sph_f.push_back(face);
			}
			face << sph_v.size()-1, sph_v.size()-1-q_sph, sph_v.size()-2;
			sph_f.push_back(face);

			// ====================================== ROUNDED SHAPE ======================================
			std::vector<Eigen::Vector3d> swept_v;
			std::vector<Eigen::Vector2i> swept_e;
			std::vector<Eigen::Vector3i> swept_f;
			// For each vertex, assign a sphere
			for(size_t i = 0; i < vertices_.size(); i++){
				// Get a quaternion that rotates the sphere so the pole points outward (looks nicer)
				Eigen::Quaterniond quat_sph;
				Eigen::Vector3d z_sph(0.0, 0.0, 1.0);
				quat_sph.setFromTwoVectors(z_sph,vertices_[i]);
				int face_index_offset = swept_v.size();
				// printf("offset is %d\n",face_index_offset);
				// Add all vertices of the sphere around each vertex
				for(size_t j = 0; j < sph_v.size(); j++){
					vertex = quat_sph * sph_v[j] + vertices_[i];
					swept_v.push_back( vertex );
				}
				// Add all faces, keeping in mind that the vertex indices will shift
				for(size_t j = 0; j < sph_f.size(); j++){
					// The .array() allows us to add a scalar value to all Eigen::Vector3i's elements
					face = sph_f[j].array() + face_index_offset;
					swept_f.push_back( face );
					// printf("vert %d face %d %d %d\n",i,face[0],face[1],face[2]);
				}
			}
			// printf("Spheres added: now have %lu vertices and %lu faces\n",swept_v.size(),swept_f.size());


			// For each edge, assign a cylinder with correct orientation and length equal to edge length
			for(size_t i = 0; i < edges_.size(); i++){
				int face_index_offset = swept_v.size();
				Eigen::Vector3d edge_vec = vertices_[ edges_[i][1] ] - vertices_[ edges_[i][0] ];
				Eigen::Vector3d edge_dir = edge_vec.normalized();
				double edge_length = edge_vec.norm();
				// Set the cylinder height
				for(size_t j = 0; j < cyl_v.size(); j++){
					if(j % 2 == 0){
						cyl_v[j][2] = -edge_length / 2.0; // Bottom
					}else{
						cyl_v[j][2] = +edge_length / 2.0; // Top
					}
				}
				// Get a quaternion that rotates the cylinder to the edge orientation
				Eigen::Quaterniond quat;
				Eigen::Vector3d z(0.0, 0.0, 1.0);
				quat.setFromTwoVectors(z,edge_dir);
				// Add the cylinder's vertices
				for(size_t j = 0; j < cyl_v.size(); j++){
					// Rotate first, then displace center of cylinder to middle of edge
					vertex = quat * cyl_v[j] + vertices_[edges_[i][0]] + 0.5 * edge_vec;
					// printf("v %f %f %f\n ",vertex[0],vertex[1],vertex[2]);
					swept_v.push_back( vertex );
				}
				// Add the cylinder's faces, keeping in mind that the indices will shift
				for(size_t j = 0; j < cyl_f.size(); j++){
					// The .array() allows us to add a scalar value to all Eigen::Vector3i's elements
					swept_f.push_back( cyl_f[j].array() + face_index_offset );
				}
			}
			// printf("Cylinders added: now have %lu vertices and %lu faces\n",swept_v.size(),swept_f.size());


			// ========================================= SAVING ==========================================
			// Open a file to write to in output truncate mode (wipes preexisting content)
			std::string file_name = "Spherocylinder_" + std::to_string(nr) + ".obj";
			std::ofstream shape_file;
		  shape_file.open(file_name, std::ios::out | std::ios::trunc);

			// Print vertices
			for(size_t i = 0; i < swept_v.size(); i++){
				shape_file << "v " << swept_v[i][0] << " " << swept_v[i][1] << " " << swept_v[i][2] << "\n";
			}

			// Print faces
			for(size_t i = 0; i < swept_f.size(); i++){
				shape_file << "f ";
				for (int j = 0; j < swept_f[i].size(); j++){
					shape_file << swept_f[i][j] << " ";
				}
				shape_file << "\n";
				// shape_file << "f " << faces_[i][0] << " " << faces_[i][1] << " " << faces_[i][2] << "\n";
			}

			shape_file.close();
			nr++;

			return " mesh " + file_name; // The return value is the specifier "mesh" plus the filename,
			// this tells the opengl code it needs to load a mesh from the file_name.
		}

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			for(Eigen::Vector3d& v : vertices_){
				v.array() *= factor;
			}
			in_radius_  *= factor;
			out_radius_ *= factor;
			diameter_   *= factor;
			length_     *= factor;
			volume_     *= pow(factor, 3);
		}

	};
}

#endif
