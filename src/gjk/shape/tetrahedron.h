#ifndef SHAPE_TETRA_H
#define SHAPE_TETRA_H

#include "convex.h"
#include <cmath>

namespace shape{

  class Tetrahedron: public Convex{
	private:
		double rib_, size_, in_radius_, out_radius_;
		double volume_;
		// Note: these vertices should always be in the particle frame of reference
		std::vector<Eigen::Vector3d> vertices_;
		std::vector<Eigen::Vector2i> edges_;
		std::vector<Eigen::Vector3i> faces_;
  public:
		// Only constuctors that specify the size are allowed
    explicit Tetrahedron(void) = delete;
    explicit Tetrahedron(char) = delete;
    explicit Tetrahedron(const double size);

    // Comparison operator overloaders
		friend bool operator==(const shape::Tetrahedron& lhs, const shape::Tetrahedron& rhs);
		friend bool operator!=(const shape::Tetrahedron& lhs, const shape::Tetrahedron& rhs);

		// Give sphereswept class access to vertices, edges and faces so we can save swept tetrehedra
		// friend class SphereSwept;

		std::string name(void)const { return "Tetrahedron"; }
    double in_radius(void)const { return in_radius_;    }
    double out_radius(void)const{ return out_radius_;   }
    double volume(void)const    { return volume_;       }

		// Getters that return vectors of vertices, edges and faces.
		const std::vector<Eigen::Vector3d>& v(void)const{	return vertices_; }
		const std::vector<Eigen::Vector2i>& e(void)const{	return edges_;    }
		const std::vector<Eigen::Vector3i>& f(void)const{	return faces_;    }

		// Saves the tetrahedron shape as a mesh to a .obj file
		std::string save_to_obj(int)const;

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			for(Eigen::Vector3d& v : vertices_){
				v.array() *= factor;
			}
			in_radius_  *= factor;
			out_radius_ *= factor;
			rib_        *= factor;
			size_       *= factor;
			volume_     *= pow(factor, 3);
		}

		// GJK support function, returns the vertex farthest in the direction of dir
    Eigen::Vector3d support(const Eigen::Vector3d&)const;
  };



	inline Eigen::Vector3d Tetrahedron::support(const Eigen::Vector3d& dir)const{
		// Vertices are
		// {+1, +1, +1},
		// {+1, -1, -1},
		// {-1, +1, -1},
		// {-1, -1, +1}

		// To recreate this function, make a Voronoi construction on the above vertices and find
		// the quickest way to find which Voronoi cell the vector dir falls into

		if(     dir[0] >= dir[1] && dir[0] >= dir[2] && -dir[1] >= dir[2]){return vertices_[1];}
		else if(dir[1] >= dir[2] && dir[1] >= dir[0] && -dir[2] >= dir[0]){return vertices_[2];}
		else if(dir[2] >= dir[0] && dir[2] >= dir[1] && -dir[0] >= dir[1]){return vertices_[3];}
		else return vertices_[0];
	}
}

#endif
