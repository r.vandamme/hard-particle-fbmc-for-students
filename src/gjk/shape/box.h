#ifndef SHAPE_BOX_H
#define SHAPE_BOX_H

#include "convex.h"

namespace shape{

  class Box: public Convex{
	private:
		Eigen::Vector3d extent_;
		double in_radius_, out_radius_;
		double volume_;

		std::vector<Eigen::Vector3d> vertices_;
		std::vector<Eigen::Vector2i> edges_;
		std::vector<Eigen::Vector3i> faces_;
  public:
		// Only constuctors that specify the size are allowed
    explicit Box(void) = delete;
    explicit Box(char) = delete;
    explicit Box(const double dim);
    explicit Box(const Eigen::Vector3d& dims);

		// Comparison operator overloaders
		friend bool operator==(const shape::Box& lhs, const shape::Box& rhs);
		friend bool operator!=(const shape::Box& lhs, const shape::Box& rhs);

		std::string name(void)const      { return "box";       }
    double in_radius(void)const      { return in_radius_;  }
    double out_radius(void)const     { return out_radius_; }
    double volume(void)const         { return volume_;     }
    Eigen::Vector3d extent(void)const{ return extent_;     }

		// Getters that return vectors of vertices, edges and faces.
		// We use getter functions here so that we can retain polymorphism of the shape::Convex class
		// since virtual data members are not possible in C++.
		const std::vector<Eigen::Vector3d>& v(void)const{ return vertices_; }
		const std::vector<Eigen::Vector2i>& e(void)const{ return edges_;    }
		const std::vector<Eigen::Vector3i>& f(void)const{ return faces_;    }

		// Function that saves the shape to a .obj file
    std::string save_to_obj(int)const;

		// GJK overlap algorithm support funcion
    Eigen::Vector3d support(const Eigen::Vector3d&)const;

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			for(Eigen::Vector3d& v : vertices_){
				v.array() *= factor;
			}
			in_radius_  *= factor;
			out_radius_ *= factor;
			volume_     *= pow(factor, 3);
			extent_.array() *= factor;
		}
  };

  inline Eigen::Vector3d Box::support(const Eigen::Vector3d& dir)const{
    return Eigen::Vector3d(
      std::copysign(extent_[0], dir[0]),
      std::copysign(extent_[1], dir[1]),
      std::copysign(extent_[2], dir[2])
    );
  }
}







#endif
