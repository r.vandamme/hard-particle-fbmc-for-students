// #include <Eigen/Dense>
#include "tetrahedron.h"
#include <algorithm>

namespace shape{

  Tetrahedron::Tetrahedron(const double rib):
    // size_(0.5 * size), // Half of the edge/rib length
    // rib_(2.0*sqrt(2.0)*size_),
		rib_(rib),
		size_(0.5 * (1.0/sqrt(2.0)) * rib_),
    in_radius_((1.0/12.0)*sqrt(6.0)*rib_),
    out_radius_(rib_*sqrt(6.0)/4.0),
    volume_(rib_*rib_*rib_/(6.0*sqrt(2.0)))
  {
		Eigen::Vector3d v;
		v << +1*size_, +1*size_, +1*size_; vertices_.push_back(v);
		v << +1*size_, -1*size_, -1*size_; vertices_.push_back(v);
		v << -1*size_, +1*size_, -1*size_; vertices_.push_back(v);
		v << -1*size_, -1*size_, +1*size_; vertices_.push_back(v);

		Eigen::Vector2i e;
		e << 0,1; edges_.push_back(e);
		e << 1,2; edges_.push_back(e);
		e << 2,0; edges_.push_back(e);
		e << 3,0; edges_.push_back(e);
		e << 3,1; edges_.push_back(e);
		e << 3,2; edges_.push_back(e);

		Eigen::Vector3i f;
		f << 0,1,2; faces_.push_back(f);
		f << 3,1,0; faces_.push_back(f);
		f << 3,2,1; faces_.push_back(f);
		f << 3,0,2; faces_.push_back(f);
	}


	// Overload for the comparison operator for this shape
	bool operator==(const shape::Tetrahedron& lhs, const shape::Tetrahedron& rhs){
		if(
			lhs.size_       == rhs.size_        &&
			lhs.rib_        == rhs.rib_         &&
			lhs.in_radius_  == rhs.in_radius_   &&
			lhs.out_radius_ == rhs.out_radius_  &&
			lhs.volume_     == rhs.volume_      &&
			lhs.vertices_   == rhs.vertices_    &&
			lhs.edges_      == rhs.edges_       &&
			lhs.faces_      == rhs.faces_
		){return true;}else{return false;}
	}
	// Inequality: calling comparison operator so that != is always the boolean opposite of ==
	bool operator!=(const shape::Tetrahedron& lhs, const shape::Tetrahedron& rhs){
		return !(lhs == rhs);
	}


	// Function to export this shape to a .obj file that can be used in e.g. visualization code
	std::string Tetrahedron::save_to_obj(int nr)const{
		// static int nr = 0;
		// Open a file to write to in output truncate mode (wipes preexisting content)
		std::string file_name = "Tetrahedron_" + std::to_string(nr) + ".obj";
		std::ofstream shape_file;
	  shape_file.open(file_name, std::ios::out | std::ios::trunc);

		// Print vertices
		for(size_t i = 0; i < vertices_.size(); i++){
			shape_file << "v " << vertices_[i][0] << " " << vertices_[i][1] << " " << vertices_[i][2] << "\n";
		}

		// Print faces
		for(size_t i = 0; i < faces_.size(); i++){
			shape_file << "f ";
			for (int j = 0; j < faces_[0].size(); j++){
				shape_file << faces_[i][j]+1 << " "; //+1 because most obj parsers start indices at 1, not 0
			}
			shape_file << "\n";
			// shape_file << "f " << faces_[i][0] << " " << faces_[i][1] << " " << faces_[i][2] << "\n";
		}

		shape_file.close();
		nr++;

		return " mesh " + file_name; // The return value is the specifier "mesh" plus the filename,
		// this tells the opengl code it needs to load a mesh from the file_name.
	}

}
