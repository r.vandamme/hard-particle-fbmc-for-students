// #include <Eigen/Dense>
#include "polyhedron.h"
#include <cstring>

namespace shape{

  inline double dot(const Eigen::Vector3d a, const Eigen::Vector3d b){
    return a.dot(b);
  }

  inline Eigen::Vector3d cross(const Eigen::Vector3d a, const Eigen::Vector3d b){
    return a.cross(b);
  }

  Polyhedron::Polyhedron(const std::vector<Eigen::Vector3d>& vertices, const std::vector<std::vector<uint>>& faces, const char* source):
    source_(nullptr), volume_(0.0), vertices_(vertices), faces_(faces)
  {
    if(source){
      source_ = new char[strlen(source) + 1];
      strcpy(source_, source);
    }
    init(vertices, faces);
  }

  Polyhedron::Polyhedron(const Polyhedron& other):
    source_(nullptr),
    in_radius_(other.in_radius_), out_radius_(other.out_radius_),
    volume_(other.volume_),
    vertices_(other.vertices_),
		edges_(other.edges_),
    faces_(other.faces_),
		triangle_faces_(other.triangle_faces_),
    vert_neighbors_(other.vert_neighbors_)
  {
    if(other.source_){
      source_ = new char[strlen(other.source_) + 1];
      strcpy(source_, other.source_);
    }
  }


  Polyhedron::~Polyhedron(void){
    delete[] source_;
  }

  Polyhedron::Polyhedron(const char* obj_file_name):
    source_(nullptr), volume_(0.0)
  {
    if(obj_file_name){
      source_ = new char[strlen(obj_file_name) + 1];
      strcpy(source_, obj_file_name);
    }
    std::vector<Eigen::Vector3d> obj_vertices;
    std::vector<std::vector<uint>> obj_faces;

    std::cout << "Loading shape from obj file: '"<<obj_file_name<<"'\n";

    // Open file for reading
    FILE* obj_file = fopen(obj_file_name,"r");

    // Check for file error
    if( !obj_file ){
        std::cout << "Failed opening file '" << obj_file_name << "' to make polyhedron. Exiting.\n";
        exit(42);
    }

    // Read data
    char lineHeader[128];
    Eigen::Vector3d vertex;
    int had_to_triangulate_faces = 0;
    while(1){
      // read the first word of the line
      int res = fscanf(obj_file, "%s", lineHeader);
      if(res == EOF){break;} // EOF = End Of File. Quit the loop.
      // If first word is "v", it's a vertex
      if( strcmp(lineHeader,"v") == 0){
         res = fscanf(obj_file, "%lf %lf %lf\n", &vertex[0], &vertex[1], &vertex[2]);
         // std::cout << vertex[0] <<" "<< vertex[1] <<" "<< vertex[2] << '\n';
         obj_vertices.push_back(vertex);
      }
      // If first word is "f", it's a face
      else if( strcmp(lineHeader,"f") == 0){ // format should be i/it/in j/jt/jn k/kt/kn
        std::vector<unsigned int> face;
        char *line = NULL, *fstr = NULL;
        size_t len = 0;
        int got = getline(&line, &len, obj_file);
        if(got) while( (fstr = strsep(&line," ")) != NULL ){
          unsigned int v_i;
          res = sscanf(fstr, "%d/%*d/%*d", &v_i); // Try format 'i/it/in'
          if(res != 1){
            res = sscanf(fstr, "%d//%*d", &v_i); // Try format 'i//in'
            if(res != 1){
              res = sscanf(fstr, "%d/%*d/", &v_i); // Try format 'i/it/'
              if(res != 1){
                res = sscanf(fstr, "%d//", &v_i); // Try format 'i//'
                if(res != 1){
                  res = sscanf(fstr, "%d", &v_i); // Try one last format 'i'
                }
              }
            }
          }
          if(res == 1){ // At least one of these formats above yielded something
            face.push_back(v_i - 1); // obj file indices start at 1, C++ indices at 0, so fix that
          }
        }
        size_t n_v = face.size(); // Code uses triangles, but obj files can also offer bigger faces
        if(n_v == 3){ // Triangles are fine, just add them
          obj_faces.push_back(face);
        } else { // Otherwise, split up the faces into triangles
          had_to_triangulate_faces = 1;
          // TODO: Might be some issues with vertex order here.
          for(unsigned int j = 1; j < n_v-1; j++){
            std::vector<unsigned int> tri_face = {face[0], face[j], face[j+1]};
            obj_faces.push_back(tri_face);
          }
        }
      }
    }
    if(had_to_triangulate_faces){
      printf("Mostly harmless warning: Had to triangulate some non-triangle mesh faces from .obj file.\n");
    }
    // Done reading
    fclose(obj_file);

    // Now call the initializer
    init(obj_vertices, obj_faces);
  }

	// Function to export this shape to a .obj file that can be used in e.g. visualization code
	std::string Polyhedron::save_to_obj(int nr)const{
		// Open a file to write to in output truncate mode (wipes preexisting content)
		std::string file_name = "Polyhedron_" + std::to_string(nr) + ".obj";
		std::ofstream shape_file;
		shape_file.open(file_name, std::ios::out | std::ios::trunc);

		// If this shape was imported from a .obj file, put this in a comment in the output .obj file
		if(source_){
			shape_file << "Shape was originally imported from .obj file named '" << source_ << "'\n";
		}

		// Print vertices
		for(size_t i = 0; i < vertices_.size(); i++){
			shape_file << "v " << vertices_[i][0] << " " << vertices_[i][1] << " " << vertices_[i][2] << "\n";
		}

		// Print faces
		for(size_t i = 0; i < faces_.size(); i++){
			shape_file << "f ";
			for (size_t j = 0; j < faces_[0].size(); j++){
				shape_file << faces_[i][j] << " ";
			}
			shape_file << "\n";
			// shape_file << "f " << faces_[i][0] << " " << faces_[i][1] << " " << faces_[i][2] << "\n";
		}

		shape_file.close();
		nr++;

		return " mesh " + file_name; // The return value is the specifier "mesh" plus the filename,
		// this tells the opengl code it needs to load a mesh from the file_name.
    }

  void Polyhedron::init(const std::vector<Eigen::Vector3d>& vertices, const std::vector<std::vector<uint>>& faces){
    using idx_t = std::vector<std::vector<uint>>::size_type;
    /* Calculate Properties */
    // for (auto v : vertices) {
    //   std::cout << v << '\n';
    // }
    // for (auto f : faces) {
    //   std::cout << "{";
    //   for (auto v : f) {
    //    std::cout << v << ',';
    //   }
    //   std::cout << "},";
    // }
    // std::cout << '\n';
    vertices_ = vertices;
    faces_ = faces;

    /* Find shared edges */
    //Iterate over each face and for each next face in the list, check if they
    //share two vertices, this defines an edge.
    std::vector< std::vector<uint> > edges;
    for(idx_t fi = 0; fi < faces.size(); ++fi){
      auto normal = cross(
        vertices_[faces[fi][1]] - vertices_[faces[fi][0]],
        vertices_[faces[fi][faces[fi].size() - 1]] - vertices_[faces[fi][0]]
      );
      normal /= normal.norm();

      auto barycenter = Eigen::Vector3d(0.0,0.0,0.0);

      double area = 0.0;
      for(idx_t fvidx = 0; fvidx < faces[fi].size(); ++fvidx){
        const auto& v1 = vertices_[faces[fi][fvidx]];
        const auto& v2 = vertices_[faces[fi][(fvidx + 1) % faces[fi].size()]];
        area += 0.5 * dot(normal, cross(v1, v2));
        barycenter += vertices_[faces[fi][fvidx]];
      }
      volume_ += fabs(dot(barycenter, normal) * area) / (3.0 * faces[fi].size());
      for(idx_t fj = fi + 1; fj < faces.size(); ++fj){
        uint fcount = 0;
        std::vector<uint> edge;
        for(auto face1_vidx: faces[fi]){
          for(auto face2_vidx: faces[fj]){
              if(face1_vidx == face2_vidx){
                edge.push_back(face1_vidx);
                ++fcount;
              }
          }
          if(fcount == 2){
            edges.push_back(edge);
            fcount = 0;
            edge.clear();
          }
        }
      }

      /* Check whether this mesh is convex */
      // We need the mesh to be convex for our overlap code to work. We check for convexity by
      // checking for each face that all vertices are on the face's hyperplane that contains the
      // origin.
      for(idx_t vidx = 0; vidx < vertices_.size(); ++vidx){
        // Exclude vertices of face that provides the normal (might be more reliable)
        // bool exclude = false;
        // for(idx_t fvidx = 0; fvidx < faces[fi].size(); ++fvidx){
        // 	if(faces[fi][fvidx] == vidx){exclude = true;}
        // }
        // if(exclude == true){break;}
        // Check hyperplane
        if( dot(normal, vertices_[vidx]) > barycenter.norm() ){
          printf("Error: Attempted to load polyhedron mesh that is not convex! Exiting.\n");
          exit(42);
        }
      }
    }

    // TODO rewrite this part of the code to work on "edges_" instead of "edges"
    // For now, just do the thing below here:
    for(size_t i = 0; i < edges.size(); i++){
      // printf("%u %u %u\n",edges[i].size(),edges[i][0],edges[i][1]);
      edges_.push_back( Eigen::Vector2i(edges[i][0],edges[i][1]) );
    }
    //printf("Found %lu edges\n", edges.size());

    /* Find Vertex Neighbours */
    //For all vertices, check if two edges share this vertex. If they do and it
    //isn't vertex 0, append the other vertices of these edge to the neighbor list
    for(uint vi = 0; vi < vertices_.size(); ++vi){
      std::vector<uint> neighbors;
      for(uint ei = 0; ei < edges.size(); ++ei){
        for(uint i = 0; i < 2; ++i){
          if(edges[ei][i] == vi && edges[ei][(i + 1) % 2] != 0) neighbors.push_back(edges[ei][(i + 1) % 2]);
        }
      }
      if(!neighbors.empty()) vert_neighbors_.push_back(neighbors);
    }

    /* Find the inscribed sphere radius */
    // For each face, calculate its distance from the particle's center and find the min
    double minDistance = 1E10;
    for(auto faceItr = faces.begin(); faceItr < faces.end(); faceItr++){
      Eigen::Vector3d p(vertices_[(*faceItr)[0]]);

      Eigen::Vector3d a(
        vertices_[(*faceItr)[1]][0] - vertices_[(*faceItr)[0]][0],
        vertices_[(*faceItr)[1]][1] - vertices_[(*faceItr)[0]][1],
        vertices_[(*faceItr)[1]][2] - vertices_[(*faceItr)[0]][2]
      );

      Eigen::Vector3d b(
        vertices_[(*faceItr)[2]][0] - vertices_[(*faceItr)[0]][0],
        vertices_[(*faceItr)[2]][1] - vertices_[(*faceItr)[0]][1],
        vertices_[(*faceItr)[2]][2] - vertices_[(*faceItr)[0]][2]
      );

      Eigen::Vector3d normal = cross(a, b);
      double length = normal.norm();
      for(int i = 0; i < 3; ++i) normal[i] /= length;
      double faceDistance = fabs(dot(normal, p));

      if(faceDistance < minDistance) minDistance = faceDistance;
    }
    in_radius_ = minDistance;
    //printf("Inscribed Radius: %f\n", in_radius_);

    /* Find the circumscribed sphere radius */
    //It's just the farthest vertex from the particle's center
    double maxDistance = 0.0;
    for(auto vItr = vertices_.begin(); vItr < vertices_.end(); vItr++){
      double vertexLength = vItr->norm();
      if( vertexLength > maxDistance) maxDistance = vertexLength;
    }
    out_radius_ = maxDistance;
    // printf("Circumscribed Radius: %f\n", out_radius_);


    // Also store faces in 3-index format
    Eigen::Vector3i face;
    for(unsigned int i = 0; i < faces_.size(); i++){
      unsigned int nv = faces_[i].size();
      if(nv > 3){ // if faces_[i] has more than 3 vertices, reduce to multiple 3-vertex faces
        for(unsigned int j = 1; j < nv-1; j++){
          face << faces_[i][0], faces_[i][j], faces_[i][j+1];
          triangle_faces_.push_back(face);
          // printf("%d %d %d\n",face[0],face[1],face[2]);
        }
      }else{ // if faces has 3 vertices, just copy
        triangle_faces_.push_back( Eigen::Vector3i(faces_[i][0],faces_[i][1],faces_[i][2]) );
      }
    }

    // std::cout << "v: " << volume_ << '\n';
  }
}
