#ifndef SHAPE_LINE_H
#define SHAPE_LINE_H

#include "convex.h"

namespace shape{

  class Line: public Convex{
	private:
		double length_;
		std::vector<Eigen::Vector3d> vertices_;
		std::vector<Eigen::Vector2i> edges_;
  public:
		// Only constuctors that specify the size are allowed
		explicit Line(void) = delete;
		explicit Line(char) = delete;
		explicit Line(double length) :
			length_(length)
		{
			vertices_.push_back( Eigen::Vector3d(-0.5 * length_, 0.0, 0.0) );
			vertices_.push_back( Eigen::Vector3d(+0.5 * length_, 0.0, 0.0) );
			edges_.push_back( Eigen::Vector2i(0,1) );
		};

    Eigen::Vector3d support(const Eigen::Vector3d& dir)const{
			// Line is 2 points: (-length_/2,0,0) and (+length_/2,0,0)
			if(dir[0] <= 0){
				return vertices_[0];
			}else{
				return vertices_[1];
			}
    }

		// getters
		std::string name(void)const { return "Line"; }
		double volume(void)const    { return 0.0; }
    double in_radius(void)const { return 0.0; }
    double out_radius(void)const{ return length_; }


		// Overload for the comparison operator for this shape
		friend bool operator==(const shape::Line& lhs, const shape::Line& rhs){
			if(
				lhs.length_   == rhs.length_   &&
				lhs.vertices_ == rhs.vertices_ &&
				lhs.edges_    == rhs.edges_
			){return true;}else{return false;}
		}
		// Inequality: calling comparison operator so that != is always the boolean opposite of ==
		friend bool operator!=(const shape::Line& lhs, const shape::Line& rhs){
			return !(lhs == rhs);
		}


		// Doesn't make sense to save lines, they're 2D embedded in 3D. Shapes that use the Line class
		// (e.g. Spherocylinder or SphereSwept) should have their own save_to_obj function.
		std::string save_to_obj(int nr)const{
			printf("ERROR: Line shape saving not yet implemented!\n");
			exit(42);
			return " line";
		}

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			for(Eigen::Vector3d& v : vertices_){
				v.array() *= factor;
			}
			length_     *= factor;
		}

		// A Line is a two vertices at (-radius_*aspect_ratio_/2,0,0) and (+radius_*aspect_ratio_/2,0,0)
		const std::vector<Eigen::Vector3d>& v(void)const{ return vertices_; }
		const std::vector<Eigen::Vector2i>& e(void)const{ return edges_; } // Just one edge: (0,1)
		const std::vector<Eigen::Vector3i>& f(void)const{ // No faces
			// printf("Error: Requested face of shape::Line class, which has no faces. Exiting!\n");
			// exit(42);
			static std::vector<Eigen::Vector3i> empty_vec;
			return empty_vec;
		}

  };
}

#endif
