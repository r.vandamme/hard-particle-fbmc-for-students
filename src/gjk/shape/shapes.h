#ifndef SHAPE_SHAPES_H
#define SHAPE_SHAPES_H

#include "box.h"
#include "polyhedron.h"
#include "tetrahedron.h"
// #include "hbox.h"
#include "sphere_swept.h"
#include "point.h"
// #include "cylinder.h"
#include "sphere.h"
#include "line.h"
#include "spherocylinder.h"

#endif
