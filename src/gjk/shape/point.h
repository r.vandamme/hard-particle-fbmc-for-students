#ifndef SHAPE_POINT_H
#define SHAPE_POINT_H

#include "convex.h"

namespace shape{

  class Point: public Convex{
  public:
    Eigen::Vector3d support(const Eigen::Vector3d& dir)const{
      return Eigen::Vector3d(0.0,0.0,0.0);
    }

		// getters
		std::string name(void)const { return "Point"; }
		double volume(void)const    { return 0.0; }
    double in_radius(void)const { return 0.0; }
    double out_radius(void)const{ return 0.0; }


		// Comparison operator overloaders
		friend bool operator==(const shape::Point& lhs, const shape::Point& rhs){return true;}
		friend bool operator!=(const shape::Point& lhs, const shape::Point& rhs){return false;}

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){ return; } // Does nothing for a point

		// Doesn't make sense to save points
		std::string save_to_obj(int nr)const{
			printf("Warning: visualization of Point and swept Point object will have incorrect radius. ");
			printf("This currently just isn't implemented. Collisions should be OK though.\n");
			return " sphere";
		}

		// A point is a single vertex at the origin in its own frame, and has no edges or faces
		const std::vector<Eigen::Vector3d>& v(void)const{ return point_; }
		const std::vector<Eigen::Vector2i>& e(void)const{
			// printf("Error: Requested edge of shape::Point class, which has no edges. Exiting!\n");
			// exit(42);
			static std::vector<Eigen::Vector2i> empty_vec;
			return empty_vec;
		}
		const std::vector<Eigen::Vector3i>& f(void)const{
			// printf("Error: Requested face of shape::Point class, which has no faces. Exiting!\n");
			// exit(42);
			static std::vector<Eigen::Vector3i> empty_vec;
			return empty_vec;
		}

	private:
		std::vector<Eigen::Vector3d> point_ = {Eigen::Vector3d(0.0, 0.0, 0.0)};
  };
}

#endif
