#ifndef SHAPE_POLYHEDRON_H
#define SHAPE_POLYHEDRON_H

#include "convex.h"
#include <cstddef>
#include <vector>
#include <algorithm>

namespace shape{
  class Polyhedron: public Convex{
	private:
		char* source_;
		double in_radius_;
		double out_radius_;
		double volume_;
		std::vector<Eigen::Vector3d> vertices_;
		std::vector<Eigen::Vector2i> edges_;
		std::vector<std::vector<unsigned int>> faces_;
		std::vector<Eigen::Vector3i> triangle_faces_; // faces with only 3 indices
		std::vector<std::vector<unsigned int>> vert_neighbors_;
    void init(const std::vector<Eigen::Vector3d>& vertices, const std::vector<std::vector<uint>>& faces);
  public:
    Polyhedron(void) : source_(nullptr){}
    explicit Polyhedron(const std::vector<Eigen::Vector3d>& vertices, const std::vector<std::vector<unsigned int>>& faces, const char* source = nullptr);
    explicit Polyhedron(const char* obj_file_name); // Load from .obj file
    Polyhedron(const Polyhedron&);
    ~Polyhedron(void);

		// Overload for the comparison operator for this shape
		friend bool operator==(const shape::Polyhedron& lhs, const shape::Polyhedron& rhs){
			if(
				lhs.vertices_       == rhs.vertices_       &&
				lhs.edges_          == rhs.edges_          &&
				lhs.vert_neighbors_ == rhs.vert_neighbors_ &&
				lhs.faces_          == rhs.faces_          &&
				lhs.in_radius_      == rhs.in_radius_      &&
				lhs.out_radius_     == rhs.out_radius_     &&
				lhs.volume_         == rhs.volume_
			){return true;}else{return false;}
		}
		// Inequality: calling comparison operator so that != is always the boolean opposite of ==
		friend bool operator!=(const shape::Polyhedron& lhs, const shape::Polyhedron& rhs){
			return !(lhs == rhs);
		}

		// Export to a .obj file
		std::string save_to_obj(int nr)const;

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			for(Eigen::Vector3d& v : vertices_){
				v.array() *= factor;
			}
			in_radius_  *= factor;
			out_radius_ *= factor;
			volume_     *= pow(factor, 3);
		}

    //TODO: Add void set_source(const char*) and const char* get_source(void) functions
    //so that we can handle the storage of an optional filename from which the shape
    //was loaded from.
    std::string name(void)const { return "Polyhedron"; }
    const char* get_source(void)const{ return source_; }
    double in_radius(void)const{  return in_radius_;  }
    double out_radius(void)const{ return out_radius_; }
    double volume(void)const{     return volume_;     }
    const std::vector<Eigen::Vector3d>& v(void)const{ return vertices_; }
    const std::vector<Eigen::Vector2i>& e(void)const{ return edges_;    }
    const std::vector<Eigen::Vector3i>& f(void)const{ return triangle_faces_;  }

    const Eigen::Vector3d& get_vertex(size_t idx)const{ return vertices_[idx]; }
    const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const{ return vert_neighbors_[idx]; }


    Eigen::Vector3d support(const Eigen::Vector3d&)const;
    double max_vert_dist2(const Eigen::Vector3d& pos, const Eigen::Quaterniond& rot)const;
};

//@note: As a future reminder, we would like to also implement a shape union.
//class UnionShape{
//    double out_radius(void)const;
//    std::vector<Shape> shapes_;
//};

	inline Eigen::Vector3d Polyhedron::support(const Eigen::Vector3d& dir)const{
    //using idx_t = std::vector<unsigned int>::size_type;
    //idx_t curr = 0;
    //double p = 0.0;
    //double max = clam::dot(vertices_[0], dir);
    //for(idx_t i = 1; i < vertices_.size(); ++i){
    //    p = clam::dot(vertices_[i], dir);
    //    if(p > max){
    //        curr = i;
    //        max = p;
    //    }
    //}
    //return vertices_[curr];
    using idx_t = std::vector<unsigned int>::size_type;
    unsigned int next = 0, last = 0, curr = 0;
    double p = 0.0;
    //double max = clam::dot(vertices_[0], dir);
    double max = vertices_[0].dot(dir);
		// Find vertex most in the direction of dir by a simple hill-climb method over each vertex's
		// neighbours.
    for(;;){
      for(idx_t vid = 0; vid < vert_neighbors_[curr].size(); ++vid){
        next = vert_neighbors_[curr][vid];
        if(next != last){
          p = vertices_[next].dot(dir);
          if(p > max){
            max = p;
            last = curr;
            curr = next;
            break;
          }
        }
        if(vid == vert_neighbors_[curr].size() - 1) return vertices_[curr];
      }
    }
  }

  inline double Polyhedron::max_vert_dist2(const Eigen::Vector3d& pos, const Eigen::Quaterniond& rot)const{
    using idx_t = std::vector<unsigned int>::size_type;
    double p = 0.0;
    Eigen::Matrix3d m(rot);
    double max = ((m * vertices_[0]) + pos).squaredNorm();
     	// double max = (rot.rotate(vertices_[0]) + pos).length2();
    for(idx_t i = 1; i < vertices_.size(); ++i){
      // p = (rot.rotate(vertices_[i]) + pos).length2();
      p = (m*vertices_[i] + pos).squaredNorm();
      if(p > max) max = p;
    }
    return max;
    //using idx_t = std::vector<unsigned int>::size_type;
    //unsigned int next = 0, last = 0, curr = 0;
    //double p = 0.0;
    //double max = (rot.rotate(vertices_[0]) + pos).length2();
    //for(;;){
    //    for(idx_t vid = 0; vid < vert_neighbors_[curr].size(); ++vid){
    //        next = vert_neighbors_[curr][vid];
    //        if(next != last){
    //            p = (rot.rotate(vertices_[next]) + pos).length2();
    //            if(p > max){
    //                max = p;
    //                last = curr;
    //                curr = next;
    //                break;
    //            }
    //        }
    //        if(vid == vert_neighbors_[curr].size() - 1) return max;
    //    }
    //}
  }
}
#endif
