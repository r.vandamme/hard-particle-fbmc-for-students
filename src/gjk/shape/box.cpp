#include "box.h"

namespace shape{
  Box::Box(const Eigen::Vector3d& dims):
    extent_(0.5 * dims),
    in_radius_(std::min(extent_[0], std::min(extent_[1], extent_[2]))),
		out_radius_(extent_.norm()),
    volume_(dims[0] * dims[1] * dims[2])
  {
    ////////////////////////
    //                    //
    //    3 +--------+ 7  //
    //     /|       /|    //
    //    / |    4 / |    //
    // 0 +--------+  |    //
    //   |  |     |  |    //
    // z |2 +-----|--+ 6  //
    //   | /      | /     //
    //   |/ y     |/      //
    // 1 +--------+ 5     //
    //       x            //
    ////////////////////////

		Eigen::Vector3d v;
		// Left
		v << -extent_[0], -extent_[1], +extent_[2]; vertices_.push_back(v);
		v << -extent_[0], -extent_[1], -extent_[2]; vertices_.push_back(v);
		v << -extent_[0], +extent_[1], -extent_[2]; vertices_.push_back(v);
		v << -extent_[0], +extent_[1], +extent_[2]; vertices_.push_back(v);
		// Right
		v << +extent_[0], -extent_[1], +extent_[2]; vertices_.push_back(v);
		v << +extent_[0], -extent_[1], -extent_[2]; vertices_.push_back(v);
		v << +extent_[0], +extent_[1], -extent_[2]; vertices_.push_back(v);
		v << +extent_[0], +extent_[1], +extent_[2]; vertices_.push_back(v);


		Eigen::Vector2i e;
		// Left
		e << 0,1; edges_.push_back(e);
		e << 1,2; edges_.push_back(e);
		e << 2,3; edges_.push_back(e);
		e << 3,0; edges_.push_back(e);
		// Right
		e << 4,5; edges_.push_back(e);
		e << 5,6; edges_.push_back(e);
		e << 6,7; edges_.push_back(e);
		e << 7,4; edges_.push_back(e);
		// Left to right
		e << 0,4; edges_.push_back(e);
		e << 1,5; edges_.push_back(e);
		e << 2,6; edges_.push_back(e);
		e << 3,7; edges_.push_back(e);


		Eigen::Vector3i f;
		// Left
		f << 2,1,0; faces_.push_back(f);
		f << 3,2,0; faces_.push_back(f);
		// Right
		f << 4,5,6; faces_.push_back(f);
		f << 4,6,7; faces_.push_back(f);
		// Bottom
		f << 6,5,1; faces_.push_back(f);
		f << 2,6,1; faces_.push_back(f);
		// Top
		f << 0,4,7; faces_.push_back(f);
		f << 0,7,3; faces_.push_back(f);
		// // Front
		f << 0,1,5; faces_.push_back(f);
		f << 0,5,4; faces_.push_back(f);
		// // Back
		f << 7,6,2; faces_.push_back(f);
		f << 3,7,2; faces_.push_back(f);
	}

	Box::Box(const double dim):
    extent_(0.5 * dim, 0.5 * dim, 0.5 * dim),
    in_radius_(std::min(extent_[0], std::min(extent_[1], extent_[2]))),
		out_radius_(extent_.norm()),
    volume_(dim * dim * dim)
  {
		////////////////////////
		//                    //
		//    3 +--------+ 7  //
		//     /|       /|    //
		//    / |    4 / |    //
		// 0 +--------+  |    //
		//   |  |     |  |    //
		// z |2 +-----|--+ 6  //
		//   | /      | /     //
		//   |/ y     |/      //
		// 1 +--------+ 5     //
		//       x            //
		////////////////////////

		Eigen::Vector3d v;
		// Left
		v << -extent_[0], -extent_[1], +extent_[2]; vertices_.push_back(v);
		v << -extent_[0], -extent_[1], -extent_[2]; vertices_.push_back(v);
		v << -extent_[0], +extent_[1], -extent_[2]; vertices_.push_back(v);
		v << -extent_[0], +extent_[1], +extent_[2]; vertices_.push_back(v);
		// Right
		v << +extent_[0], -extent_[1], +extent_[2]; vertices_.push_back(v);
		v << +extent_[0], -extent_[1], -extent_[2]; vertices_.push_back(v);
		v << +extent_[0], +extent_[1], -extent_[2]; vertices_.push_back(v);
		v << +extent_[0], +extent_[1], +extent_[2]; vertices_.push_back(v);


		Eigen::Vector2i e;
		// Left
		e << 0,1; edges_.push_back(e);
		e << 1,2; edges_.push_back(e);
		e << 2,3; edges_.push_back(e);
		e << 3,0; edges_.push_back(e);
		// Right
		e << 4,5; edges_.push_back(e);
		e << 5,6; edges_.push_back(e);
		e << 6,7; edges_.push_back(e);
		e << 7,4; edges_.push_back(e);
		// Left to right
		e << 0,4; edges_.push_back(e);
		e << 1,5; edges_.push_back(e);
		e << 2,6; edges_.push_back(e);
		e << 3,7; edges_.push_back(e);


		Eigen::Vector3i f;
		// Left
		f << 2,1,0; faces_.push_back(f);
		f << 3,2,0; faces_.push_back(f);
		// Right
		f << 4,5,6; faces_.push_back(f);
		f << 4,6,7; faces_.push_back(f);
		// Bottom
		f << 6,5,1; faces_.push_back(f);
		f << 2,6,1; faces_.push_back(f);
		// Top
		f << 0,4,7; faces_.push_back(f);
		f << 0,7,3; faces_.push_back(f);
		// // Front
		f << 0,1,5; faces_.push_back(f);
		f << 0,5,4; faces_.push_back(f);
		// // Back
		f << 7,6,2; faces_.push_back(f);
		f << 3,7,2; faces_.push_back(f);
	}




	// Overload for the comparison operator for this shape
	bool operator==(const shape::Box& lhs, const shape::Box& rhs){
		if(
			lhs.extent_     == rhs.extent_ &&
			lhs.in_radius_  == rhs.in_radius_ &&
			lhs.out_radius_ == rhs.out_radius_ &&
			lhs.volume_     == rhs.volume_
		){return true;}else{return false;}
	}
	// Inequality: calling comparison operator so that != is always the boolean opposite of ==
	bool operator!=(const shape::Box& lhs, const shape::Box& rhs){
		return !(lhs == rhs);
	}


	// Function to export this shape to a .obj file that can be used in e.g. visualization code
	std::string Box::save_to_obj(int nr)const{
		// static int nr = 0;
		// Open a file to write to in output truncate mode (wipes preexisting content)
		std::string file_name = "Box_" + std::to_string(nr) + ".obj";
		std::ofstream shape_file;
	  shape_file.open(file_name, std::ios::out | std::ios::trunc);

		// Print verticles
		for(size_t i = 0; i < vertices_.size(); i++){
			shape_file << "v " << vertices_[i][0] << " " << vertices_[i][1] << " " << vertices_[i][2] << "\n";
		}

		// Print faces_
		for(size_t i = 0; i < faces_.size(); i++){
			shape_file << "f ";
			for (int j = 0; j < faces_[0].size(); j++){
				shape_file << faces_[i][j] << " ";
			}
			shape_file << "\n";
			// shape_file << "f " << faces_[i][0] << " " << faces_[i][1] << " " << faces_[i][2] << "\n";
		}

		shape_file.close();
		nr++;

		return " mesh " + file_name; // The return value is the specifier "mesh" plus the filename,
		// this tells the opengl code it needs to load a mesh from the file_name.
	}

}
