#ifndef SHAPE_SPHERE_SWEPT_H
#define SHAPE_SPHERE_SWEPT_H

#include "convex.h"
#include "../transform.h"

namespace shape{
 	// NOTE: Class marked final as inheritance would currently break it, mainly due to the
	// overloaded comparison (==) operator
  class SphereSwept final: public Convex{
  private:
    shape::Convex* shape_; // For initialization, the derived shape must be owned by sphere_swept!
    double radius_;
    double in_radius_, out_radius_;
    double volume_;
  public:
    explicit SphereSwept(shape::Convex*, double rounding_ratio);
		~SphereSwept(void){delete shape_;}
    Eigen::Vector3d support(const Eigen::Vector3d&)const;

		// Overload the comparison operators so that we can correctly compare shapes
		friend bool operator==(const shape::SphereSwept& lhs, const shape::SphereSwept& rhs);
		friend bool operator!=(const shape::SphereSwept& lhs, const shape::SphereSwept& rhs);

		// Getters
		double radius(void)const    {	return radius_;	}
		std::string name(void)const { return "SphereSwept";	}
		double volume(void)const    {	return volume_; }
		// In_radius and out_radius are those of the shape plus the swept radius
		double in_radius(void)const { return shape_->in_radius()  + radius_; }
		double out_radius(void)const{	return shape_->out_radius() + radius_; }

		// Save the swept shape
		std::string save_to_obj(int nr)const;

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
			shape_->scale_by_factor(factor);
			radius_     *= factor;
			in_radius_  *= factor;
			out_radius_ *= factor;
			volume_     *= pow(factor, 3);
		}

		// These currently return the vertices, edges and faces of the base shape and a warning.
		const std::vector<Eigen::Vector3d>& v(void)const{
			printf("Warning: Requested vertices of sphereswept shape, returning those of base shape!\n");
			return shape_->v();
		}
		const std::vector<Eigen::Vector2i>& e(void)const{
			printf("Warning: Requested edges of sphereswept shape, returning those of base shape!\n");
			return shape_->e();
		}
		const std::vector<Eigen::Vector3i>& f(void)const{
			printf("Warning: Requested faces of sphereswept shape, returning those of base shape!\n");
			return shape_->f();
		}
  };


  inline Eigen::Vector3d SphereSwept::support(const Eigen::Vector3d& dir)const{
    return shape_->support(dir) + radius_ * dir.normalized();
  }
}







#endif
