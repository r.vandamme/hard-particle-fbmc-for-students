#ifndef SHAPE_CONVEX_H
#define SHAPE_CONVEX_H


// #include <eigen3/Eigen/Geometry>
#include "../../../external/eigen3/Eigen/Geometry"
#include <string>
#include <fstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

namespace shape{

  class Convex{
  public:
    virtual ~Convex(void){};
    virtual Eigen::Vector3d support(const Eigen::Vector3d&)const = 0;
		virtual std::string name(void)const = 0;
    virtual double volume(void)const = 0;
		virtual double in_radius(void)const = 0;
		virtual double out_radius(void)const = 0;
    virtual std::string save_to_obj(int)const = 0; // Makes a file that defines the shape
		virtual void scale_by_factor(double) = 0;

		// Overloaded comparison operator. First compares whether the derived class is the same,
		// then calls their own overloaded operators to see whether the instances of these derived
		// classes have the same parameters. NOTE: Internet says this is dangerous, why?
		virtual bool operator==(const Convex& other)const{
      if(typeid(*this) != typeid(other)){
        return false;
			}
      return *this == other;
    }

		// Virtual functions to obtain the vertices and edges of an object, if it has any.
		virtual const std::vector<Eigen::Vector3d>& v(void)const=0;
		virtual const std::vector<Eigen::Vector2i>& e(void)const=0;
		virtual const std::vector<Eigen::Vector3i>& f(void)const=0;

  };
}

#endif
