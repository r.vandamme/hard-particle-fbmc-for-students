#ifndef SHAPE_SPHERE_H
#define SHAPE_SPHERE_H

#include "convex.h"

namespace shape{

  class Sphere: public Convex{
	private:
		std::vector<Eigen::Vector3d> point_ = {Eigen::Vector3d(0.0, 0.0, 0.0)};
		double radius_;
		double volume_;
		double in_radius_;
		double out_radius_;
  public:
		// Only constuctors that specify the size are allowed
		explicit Sphere(void) = delete;
		explicit Sphere(char) = delete;
		explicit Sphere(const double diameter) :
			radius_(0.5*diameter),
			volume_(4.0*M_PI/3.0*pow(radius_,3)),
			in_radius_(radius_), out_radius_(radius_)
			{};

		// We call the SphereSwept(Point,radius_) 's support to get the right result
    Eigen::Vector3d support(const Eigen::Vector3d& dir)const{
			return radius_ * dir.normalized();
    }

		// getters
		std::string name(void)const { return "Sphere"; }
		double volume(void)const    { return volume_; }
    double in_radius(void)const { return in_radius_; }
    double out_radius(void)const{ return out_radius_; }


		// Comparison operator overloaders
		friend bool operator==(const shape::Sphere& lhs, const shape::Sphere& rhs){
			if(
				lhs.radius_     == rhs.radius_    &&
				lhs.volume_     == rhs.volume_    &&
				lhs.in_radius_  == rhs.in_radius_ &&
				lhs.out_radius_ == rhs.out_radius_
			){return true;}else{return false;}
		};
		friend bool operator!=(const shape::Sphere& lhs, const shape::Sphere& rhs){
			return !(lhs == rhs);
		};


		// Spheres have special code in partViewer3D, so just provide the keyword "sphere"
		std::string save_to_obj(int nr)const{
			std::string output = " sphere " + std::to_string(radius_);
			return output; // Mind the lowercase, partViewer3D is caps sensitive
		}

		// Function that scales the shape's size by a factor
		void scale_by_factor(double factor){
      radius_     *= factor;
			in_radius_  *= factor;
			out_radius_ *= factor;
			volume_     *= pow(factor, 3);
		}

		// A point is a single vertex at the origin in its own frame, and has no edges or faces
		const std::vector<Eigen::Vector3d>& v(void)const{ return point_; }
		const std::vector<Eigen::Vector2i>& e(void)const{
			// printf("Error: Requested edge of shape::Sphere class, which has no edges. Exiting!\n");
			// exit(42);
			static std::vector<Eigen::Vector2i> empty_vec;
			return empty_vec;
		}
		const std::vector<Eigen::Vector3i>& f(void)const{
			// printf("Error: Requested face of shape::Sphere class, which has no faces. Exiting!\n");
			// exit(42);
			static std::vector<Eigen::Vector3i> empty_vec;
			return empty_vec;
		}
  };
}

#endif
