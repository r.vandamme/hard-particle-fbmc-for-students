#include "sphere_swept.h"

namespace shape{

	// Overload for the comparison operator for this shape
	bool operator==(const shape::SphereSwept& lhs, const shape::SphereSwept& rhs){
		if(
			lhs.shape_      == rhs.shape_      && // Poll equality of non-swept shape through its overloaded operator
			lhs.radius_     == rhs.radius_     &&
			lhs.in_radius_  == rhs.in_radius_  &&
			lhs.out_radius_ == rhs.out_radius_ &&
			lhs.volume_     == rhs.volume_
		){return true;}else{return false;}
	}
	// Inequality: calling comparison operator so that != is always the boolean opposite of ==
	bool operator!=(const shape::SphereSwept& lhs, const shape::SphereSwept& rhs){
		return !(lhs == rhs);
	}


	/* Constructor also calculates the volume of the swept shape */
  SphereSwept::SphereSwept(shape::Convex* shape, double rounding_ratio):
    shape_(shape)
	{
		// Calculate the rounding radius from the input ratio
		radius_ = rounding_ratio * shape->out_radius() / (1.0 - rounding_ratio);

		// We need to calculate the volume of the swept shape. For this we calculate the volume of
		// the cylinder sections around each edge shared by a pair of faces, the volume of the
		// spherical sections around each vertex, and the extra volume from moving the faces outward.
		const std::vector<Eigen::Vector3d>& vertices = shape->v();
		const std::vector<Eigen::Vector2i>& edges = shape->e();
		const std::vector<Eigen::Vector3i>& faces = shape->f();

		// The methods below don't work for shapes that have fewer vertices/edges/faces than the simplex
		// of 3D space (a tetrahedron). So shapes from lower dimensionality embedded in 3D space aren't
		// supported. There are two exceptions we would like to use: a sphere (1 vertex, 0 edges, 0 faces),
		// and a spherocylinder (2 vertices, 1 edge, 0 faces) are defined as a point and a line with a
		// thickness, respectively. We check for these and do them manually.
		if( vertices.size() < 4 || edges.size() < 6 || faces.size() < 4){
			if( vertices.size() == 1 && edges.size() == 0 && faces.size() == 0){
				// Point case: a sphere
				volume_ = (4.0/3.0) * M_PI * radius_ * radius_ * radius_;
				return;
			}else if (vertices.size() == 2 && edges.size() == 1 && faces.size() == 0){
				// Line case: a spherocylinder
				volume_ = (4.0/3.0) * M_PI * radius_ * radius_ * radius_;
				double edge_length = (vertices[edges[0][1]] - vertices[edges[0][0]]).norm();
				volume_ += M_PI * radius_ * radius_ * edge_length;
				return;
			}else{
				// Invalid shape case
				std::string error =
				"Error: Dimensionality of shape requested to be sphereswept is too low: fewer vertices, edges"
				" or faces than the simplex of 3D real space (a tetrahedron). This can be due to an error"
				" in the requested shape, or due to the requested shape being an embedded shape from a lower"
				" dimensionality (e.g. a triangle or square). The latter shapes are not supported, sorry.\n";
				std::cout << error << '\n';
				std::cout << "Vertices, edges and faces of requested shape: ";
				std::cout << vertices.size() <<" "<< edges.size() <<" "<< faces.size();
				std::cout << ". Minimum number should be 4, 6, 4 respectively. Exiting now." << '\n';
				exit(42);
			}
		}

		// Start by setting volume equal to that of the shape we're sweeping
		volume_ = shape->volume();
		// printf("volume of non-swept shape %f\n",volume_);
		// If this volume is 0, we either have an invalid shape (e.g. non-convex) or its size is 0. We
		// would like to have this latter case work for testing, occasionally. But the method below will
		// spit out NaN's in that case. For any shape of size 0, the swept shape is a sphere, so we just
		// manually define its volume:
		if(volume_ == 0){
			volume_ = (4.0/3.0) * M_PI * radius_ * radius_ * radius_;
			return;
		}

		// =================================== CYLINDER SEGMENTS =======================================

		// We first find for all edges the faces that share that edge and store these. This map holds
		// the indices of the two faces that share an edge with the same index as the vector index.
		// i.e. edge 0 is shared by faces 0 and 1 means ef_index_map[0] = {0,1}.
		std::vector<Eigen::Vector2i> ef_index_map( edges.size() );
		for(size_t e_idx = 0; e_idx < edges.size(); e_idx++){
			const Eigen::Vector2i& e = edges[e_idx];
			// For every edge, find the two faces that share this edge (i.e. contain its two vertices)
			int found_faces = 0;
			for(size_t f_idx = 0; f_idx < faces.size(); f_idx++){
				const Eigen::Vector3i& f = faces[f_idx];
				int shared_vertex_indices = 0;
				for(int i = 0; i < f.size(); i++){
					int f_v_idx = f[i];
					for(int j = 0; j < e.size(); j++){
						int e_v_idx = e[j];
						if(f_v_idx == e_v_idx) shared_vertex_indices++;
					}
					if(shared_vertex_indices == 2){
						ef_index_map[e_idx][found_faces] = f_idx;
						found_faces++;
						break;
					}
				}
				if(found_faces == 2) break;
			}
		}
    // for(size_t i = 0; i < ef_index_map.size(); i++){
    //   printf("%lu %d %d\n",i,ef_index_map[i][0],ef_index_map[i][1]);
    // }

		// Now that we have the faces of every edge, we calculate the angle between their normals
		for(size_t e_idx = 0; e_idx < edges.size(); e_idx++){
			Eigen::Vector2i& ef_pair = ef_index_map[e_idx];
			double edge_length = (vertices[edges[e_idx][1]] - vertices[edges[e_idx][0]]).norm();
			if(edge_length == 0) continue; // <-- This makes more sense than spitting out NaN's, imo.
			// Normal of the first face in the map: cross product of the edges from its vertices
			const Eigen::Vector3i& f1 = faces[ef_pair[0]];
			Eigen::Vector3d f1e1 = vertices[f1[1]] - vertices[f1[0]];
			Eigen::Vector3d f1e2 = vertices[f1[2]] - vertices[f1[0]];
			Eigen::Vector3d n1 = f1e1.cross(f1e2).normalized();
			// Normal of the second face in the map: cross product of the edges from its vertices
			const Eigen::Vector3i& f2 = faces[ef_pair[1]];
			Eigen::Vector3d f2e1 = vertices[f2[1]] - vertices[f2[0]];
			Eigen::Vector3d f2e2 = vertices[f2[2]] - vertices[f2[0]];
			Eigen::Vector3d n2 = f2e1.cross(f2e2).normalized();
			// Angle between the normals divided by 2 pi gives the fraction of the sphereswept cylinder
			// around the edge. Its volume is then easily calculated.
			double x = n1.dot(n2) / (n1.norm()*n2.norm());
			if(x<-1.0){x=-1.0;}else if(x>1.0){x=1.0;} // Fix precision errors with (anti)parallel normals
			double angle = acos(x);
			volume_ += M_PI * radius_ * radius_ * edge_length * (angle / (2.0 * M_PI));
		}
		// printf("volume after es %f\n",volume_);

		// ================================== SPHERE SEGMENTS ==========================================

		// For the spherical segments, we first make a list of the faces of each vertex
		std::vector<std::vector<int>> vf_index_map( vertices.size() );
		for(size_t v_idx = 0; v_idx < vertices.size(); v_idx++){
			// The index of every face that contains this vertex index gets added to the list
			for(size_t f_idx = 0; f_idx < faces.size(); f_idx++){
			const Eigen::Vector3i& f = faces[f_idx];
				for(int i = 0; i < f.size(); i++){
					unsigned int f_v_idx = f[i];
					if(f_v_idx == v_idx) vf_index_map[v_idx].push_back(f_idx);
				}
			}
		}
    // for(size_t i = 0; i < vf_index_map.size(); i++){
    //   printf("%lu ",i);
    //   for(size_t j = 0; j < vf_index_map[i].size(); j++){
    //     printf("%d ",vf_index_map[i][j]);
    //   }
    //   printf("\n");
    // }

		// We now generate a list of points for each vertex: for each connected face we make a new point
		// on the new sphereswept surface which is on the border of the spherical segment. This point is
		// the intersection of the cylinder segments we made above, and also one of the vertices of the
		// extended faces.
		for(size_t v_idx = 0; v_idx < vertices.size(); v_idx++){
			std::vector<Eigen::Vector3d> segment_vertices;
			for(int& f_idx : vf_index_map[v_idx]){
				// Normal of the face: cross product of the edges from its vertices
				const Eigen::Vector3i& f = faces[f_idx];
				Eigen::Vector3d fe1 = vertices[f[1]] - vertices[f[0]];
				Eigen::Vector3d fe2 = vertices[f[2]] - vertices[f[0]];
				Eigen::Vector3d n = fe1.cross(fe2).normalized();
				// Add the new vertex point to the list
				Eigen::Vector3d new_v = vertices[v_idx] + (radius_*n.array()).matrix();
				segment_vertices.push_back(new_v);
			}

			// We now construct tetrahedra OABC, where O is the old vertex position and ABC are taken from
			// the set of points we just constructed. For each of these (non-overlapping) tetrahedra, we
			// find the solid angle of the ABC triangle and use that to calculate the fraction of a full
			// sphere that this segment occupies. By iterating over all these tetrahedra we get the volume
			// of the entire spherical segment sweeping this vertex.
			const Eigen::Vector3d& v_O = vertices[v_idx];
			double solid_angle = 0.0;
			for(size_t idx = 0; idx < segment_vertices.size()-2; idx++){
        Eigen::Vector3d v_A = segment_vertices[idx]   - v_O; double a = v_A.norm();
				Eigen::Vector3d v_B = segment_vertices[idx+1] - v_O; double b = v_B.norm();
				Eigen::Vector3d v_C = segment_vertices[idx+2] - v_O; double c = v_C.norm();
				Eigen::Matrix3d m;
				m << v_A, v_B, v_C;
				double triple_product = fabs(m.determinant());
				// printf("triple %f\n",triple_product);
				if(triple_product == 0) continue; // Prevent NaN's if e.g. 2 vertices are identical
				solid_angle += 2.0 * atan2(triple_product, (a*b*c + v_A.dot(v_B)*c + v_B.dot(v_C)*a + v_C.dot(v_A)*b));
			}
			// Now we have the total solid angle, we calculate the volume of the total sphere segment
			volume_ += (4.0/3.0) * M_PI * radius_ * radius_ * radius_ * (solid_angle / (4.0 * M_PI));
		}
		// printf("volume after vs %f\n",volume_);

		// =================================== FACES SEGMENTS ==========================================

		// Finally, we need to calculate the increase in volume from moving the faces outward. This we
		// can do by first calculating the 2D area of the face, and then simply multiplying by the
		// swept radius.
		for(const Eigen::Vector3i& f : faces){
			// Calculate area
			Eigen::Vector3d e1 = vertices[f[1]] - vertices[f[0]];
			Eigen::Vector3d e2 = vertices[f[2]] - vertices[f[0]];
			double f_area = 0.5 * e1.cross(e2).norm();
			if(std::isnan(f_area)) continue; // Skip if edges are poorly defined e.g. zero length.
			// Add extra volume to the new total
			volume_ += radius_ * f_area;
		}
		// printf("volume after fs %f\n",volume_);
	}




	/* To save the swept shape, we create an .obj file where we pad the non-swept shape with spheres
	and cylinders: every vertex of the non-swept shape gets a sphere and every edge a cylinder. The
	radius of these is the swept radius. */
	std::string SphereSwept::save_to_obj(int nr)const{
		// Treat point particles differently cause the openGL code supports this.
		// Preferably, people would just use the Sphere class instead of this...
		if(shape_->name() == "Point"){return shape_->save_to_obj(nr);}


		double q_cyl = 18; // How round our cylinders are. Higher=rounder.
		unsigned int q_sph = 18; // Roundness of spheres. Higher=rounder. Don't set too high! Keep even.

		// Protect somewhat against crazy users that want rounded objects of very complex shapes
		// if(shape_->v().size() > 100){q_sph = 6;}
		// if(shape_->e().size() > 100){q_cyl = 6;}

		Eigen::Vector3d vertex;
		Eigen::Vector3i face;

		// ====================================== CYLINDER ======================================
		// Make vertices and faces of a cylinder in z of height 0 (we set the height later)
		std::vector<Eigen::Vector3d> cyl_v;
		std::vector<Eigen::Vector3i> cyl_f;
		// First two vertices added manually: one vertex for the bottom, one for the top.
		vertex << radius_, 0.0, 0.0;
		cyl_v.push_back(vertex);
		cyl_v.push_back(vertex);
		// Now the rest, and make the faces too
		for(double phi = (2.0*M_PI / (double)q_cyl); phi < 2.0*M_PI; phi += (2.0*M_PI / (double)q_cyl) ){
			vertex << radius_*cos(phi), radius_*sin(phi), 0.0;
			// One vertex for the bottom, one for the top
			cyl_v.push_back(vertex);
			cyl_v.push_back(vertex);
			// Make the new faces (vertices are in clockwise order)
			// face << cyl_v.size()-4, cyl_v.size()-3, cyl_v.size()-2;
			// cyl_f.push_back(face);
			// face << cyl_v.size()-1, cyl_v.size()-2, cyl_v.size()-3;
			// cyl_f.push_back(face);
			// Make the new faces (vertices are in anticlockwise order)
			face << cyl_v.size()-2, cyl_v.size()-3, cyl_v.size()-4;
			cyl_f.push_back(face);
			face << cyl_v.size()-3, cyl_v.size()-2, cyl_v.size()-1;
			cyl_f.push_back(face);
		}
		// Connect the last faces
		face << 0, cyl_v.size()-1, cyl_v.size()-2;
		cyl_f.push_back(face);
		face << cyl_v.size()-1, 0, 1;
		cyl_f.push_back(face);


		// ====================================== SPHERE ======================================
		// Method 1 for sphere: Make vertices and faces of the sphere by discretizing the angles
		std::vector<Eigen::Vector3d> sph_v;
		std::vector<Eigen::Vector3i> sph_f;
		// Top pole manually to prevent degeneracy
		sph_v.push_back( Eigen::Vector3d(0.0, 0.0, radius_) );
		// Then the rest
		for(double theta = (M_PI / (double)q_sph); theta < M_PI; theta += (M_PI / (double)q_sph) ){
			for(double phi = 0; phi < 2.0*M_PI; phi += (2.0*M_PI / (double)q_sph) ){
				// Add a vertex of the sphere
				vertex << radius_*sin(theta)*cos(phi), radius_*sin(theta)*sin(phi), radius_*cos(theta);
				sph_v.push_back(vertex);
			}
		}
		// Bottom pole manually to prevent degeneracy
		sph_v.push_back( Eigen::Vector3d(0.0, 0.0, -radius_) );
		// Face indices, top pole first
		for(size_t j = 1; j < q_sph; j++){
			face << 0, j, j+1;
			sph_f.push_back(face);
		}
		face << 0, q_sph, 1;
		sph_f.push_back(face);
		// Then bulk
		for(size_t i = 1; i < q_sph-1; i++){
			for(size_t j = 0; j < q_sph; j++){ // 1+(i-1)*q_sph is because pole is 1 vertex
				face << 1+(i-1)*q_sph + j, 1+i*q_sph + j, 1+i*q_sph + j+1;
				sph_f.push_back(face);
				face << 1+(i-1)*q_sph + j, 1+i*q_sph + j+1, 1+(i-1)*q_sph + j+1;
				sph_f.push_back(face);
			}
			// Connect first vertices to last vertices in phi direction
			face << 1+(i-1)*q_sph + q_sph-1, 1+i*q_sph + q_sph-1, 1+i*q_sph + 0;
			sph_f.push_back(face);
			face << 1+(i-1)*q_sph + q_sph-1, 1+i*q_sph + 0, 1+(i-1)*q_sph + 0;
			sph_f.push_back(face);
		}
		// Then the bottom pole
		for(size_t j = sph_v.size()-1; j > sph_v.size() - q_sph; j--){
			face << sph_v.size()-1, j-1, j-2;
			sph_f.push_back(face);
		}
		face << sph_v.size()-1, sph_v.size()-1-q_sph, sph_v.size()-2;
		sph_f.push_back(face);

		// Method 2 for sphere: Make vertices and faces of a sphere by recursive subdivision
		// NOTE: Does create a sphere, but it looks extremely janky. Kinda cool, though.
		// std::vector<Eigen::Vector3d> sph_v;
		// std::vector<Eigen::Vector3i> sph_f;
		// // Start with a tetrahedron on the unit sphere
		// vertex << radius_ * +sqrt(8.0/9.0), radius_ * 0.0           , radius_ * -1.0/3.0; sph_v.push_back(vertex);
		// vertex << radius_ * -sqrt(2.0/9.0), radius_ * +sqrt(2.0/3.0), radius_ * -1.0/3.0; sph_v.push_back(vertex);
		// vertex << radius_ * -sqrt(2.0/9.0), radius_ * -sqrt(2.0/3.0), radius_ * -1.0/3.0; sph_v.push_back(vertex);
		// vertex << radius_ * 0.0           , radius_ * 0.0           , radius_ * +1.0;     sph_v.push_back(vertex);
		// face << 0,1,2;	sph_f.push_back(face);
		// face << 3,1,0;	sph_f.push_back(face);
		// face << 3,2,1;	sph_f.push_back(face);
		// face << 3,0,2;	sph_f.push_back(face);
		// // Then make a new vertex in the middle of each face, normalize it, and create new faces
		// // Do this multiple times for better quality (more spherical)
		// for(size_t q = 1; q <= q_sph; q++){
		// 	size_t n_f = sph_f.size();
		// 	for(size_t i = 0; i < n_f; i++){
		// 		// First calculate the center of the face by averaging over its vertices
		// 		vertex << 0.0, 0.0, 0.0;
		// 		for(int j = 0; j < sph_f[i].size(); j++){
		// 			vertex += sph_v[sph_f[i][j]];
		// 		}
		// 		// Then normalize it, multiply by radius, and add it to the list
		// 		sph_v.push_back( radius_ * vertex.normalized() );
		// 		// Then add the new faces (this should work for any number of vertices per face)
		// 		for(int j = 0; j < sph_f[i].size(); j++){
		// 			if(j == sph_f[i].size() - 1){ // Last to first vertex needs special care
		// 				face << sph_f[i][j], sph_v.size()-1, sph_f[i][0];
		// 				sph_f.push_back(face);
		// 			}else{
		// 				face << sph_f[i][j], sph_v.size()-1, sph_f[i][j+1];
		// 				sph_f.push_back(face);
		// 			}
		// 		}
		// 	}
		// 	// Remove the old faces
		// 	sph_f.erase(sph_f.begin(), sph_f.begin() + n_f);
		// 	// printf("iteration %d has %d vertices, %d faces. \n",q,sph_v.size(),sph_f.size());
		// }

		// Method 3 for sphere (lazy way): Take some large polyhedron from the internet and use that
		// See "SphericalApproximant.nb" mathematica notebook. This is a Pentakis_icosidodecahedron from
		// http://www.georgehart.com/virtual-polyhedra/conway_notation.html, enter "k5aD".
		// double verts[42][3] = {
		// 	{0.356825,0.,0.934171},{0.755761,0.309023,0.577348},{0.467086,0.80902,0.356815},{-0.178413,0.309013,0.934173},
		// 	{-0.645501,0.499999,0.577348},{-0.934171,0.,0.356825},{-0.178413,-0.309013,0.934173},{-0.110261,-0.809017,0.577351},
		// 	{0.755763,-0.309014,0.577349},{0.866028,-0.499996,0.},{-0.110271,0.809016,0.577351},{0.,1.,0.},
		// 	{-0.645501,-0.499999,0.577348},{-0.866025,-0.500001,0.},{0.866025,0.500001,0.},{0.645501,0.499999,-0.577348},
		// 	{0.467088,-0.809013,0.356828},{0.,-1.,0.},{0.110271,-0.809016,-0.577351},{-0.866028,0.499996,0.},
		// 	{-0.755763,0.309014,-0.577349},{0.934171,0.,-0.356825},{0.645501,-0.499999,-0.577348},
		// 	{0.178413,-0.309013,-0.934173},{-0.467088,0.809013,-0.356828},{0.110261,0.809017,-0.577351},
		// 	{0.178413,0.309013,-0.934173},{-0.467086,-0.80902,-0.356815},{-0.755761,-0.309023,-0.577348},
		// 	{-0.356825,0.,-0.934171},{0.303529,0.525736,0.794652},{-0.60706,0.,0.794656},{0.30353,-0.525728,0.794657},
		// 	{0.982246,0.,0.187595},{-0.491127,0.850648,0.187596},{-0.491122,-0.850651,0.187595},
		// 	{0.491122,0.850651,-0.187595},{0.491127,-0.850648,-0.187596},{-0.982246,0.,-0.187595},
		// 	{0.60706,0.,-0.794656},{-0.30353,0.525728,-0.794657},{-0.303529,-0.525736,-0.794652}
		// };
		// std::vector<Eigen::Vector3d> sph_v(verts, verts + sizeof(verts)/sizeof(*verts));
		// int faces[80][3] = {
		// 	{10,4,3},{3,6,0},{0,8,1},{1,14,2},{2,11,10},{12,7,6},{4,19,5},{5,13,12},{7,17,16},{16,9,8},{9,22,21},
		// 	{21,15,14},{11,25,24},{24,20,19},{13,28,27},{27,18,17},{15,26,25},{18,23,22},{20,29,28},{23,29,26},
		// 	{3,30,10},{0,30,3},{1,30,0},{2,30,1},{10,30,2},{6,31,12},{3,31,6},{4,31,3},{5,31,4},{12,31,5},
		// 	{0,32,8},{6,32,0},{7,32,6},{16,32,7},{8,32,16},{1,33,14},{8,33,1},{9,33,8},{21,33,9},{14,33,21},
		// 	{4,34,19},{10,34,4},{11,34,10},{24,34,11},{19,34,24},{7,35,17},{12,35,7},{13,35,12},{27,35,13},
		// 	{17,35,27},{2,36,11},{14,36,2},{15,36,14},{25,36,15},{11,36,25},{9,37,22},{16,37,9},{17,37,16},
		// 	{18,37,17},{22,37,18},{5,38,13},{19,38,5},{20,38,19},{28,38,20},{13,38,28},{15,39,26},{21,39,15},
		// 	{22,39,21},{23,39,22},{26,39,23},{20,40,29},{24,40,20},{25,40,24},{26,40,25},{29,40,26},
		// 	{18,41,23},{27,41,18},{28,41,27},{29,41,28},{23,41,29}
		// };
		// std::vector<Eigen::Vector3i> sph_f(faces, faces + sizeof(faces)/sizeof(*faces));

		// ====================================== ROUNDED SHAPE ======================================
		std::vector<Eigen::Vector3d> swept_v;
		std::vector<Eigen::Vector2i> swept_e;
		std::vector<Eigen::Vector3i> swept_f;


		// For each vertex, assign a sphere
		for(size_t i = 0; i < shape_->v().size(); i++){
			// Get a quaternion that rotates the sphere so the pole points outward (looks nicer)
			Eigen::Quaterniond quat_sph;
			Eigen::Vector3d z_sph(0.0, 0.0, 1.0);
			quat_sph.setFromTwoVectors(z_sph,shape_->v()[i]);
			int face_index_offset = swept_v.size();
			// printf("offset is %d\n",face_index_offset);
			// Add all vertices of the sphere around each vertex
			for(size_t j = 0; j < sph_v.size(); j++){
				vertex = quat_sph * sph_v[j] + shape_->v()[i];
				swept_v.push_back( vertex );
			}
			// Add all faces, keeping in mind that the vertex indices will shift
			for(size_t j = 0; j < sph_f.size(); j++){
				// The .array() allows us to add a scalar value to all Eigen::Vector3i's elements
				face = sph_f[j].array() + face_index_offset;
				swept_f.push_back( face );
				// printf("vert %d face %d %d %d\n",i,face[0],face[1],face[2]);
			}
		}
		// printf("Spheres added: now have %lu vertices and %lu faces\n",swept_v.size(),swept_f.size());


		// For each edge, assign a cylinder with correct orientation and length equal to edge length
		for(size_t i = 0; i < shape_->e().size(); i++){
			int face_index_offset = swept_v.size();
			Eigen::Vector3d edge_vec = shape_->v()[ shape_->e()[i][1] ] - shape_->v()[ shape_->e()[i][0] ];
			Eigen::Vector3d edge_dir = edge_vec.normalized();
			double edge_length = edge_vec.norm();
			// Set the cylinder height
			for(size_t j = 0; j < cyl_v.size(); j++){
				if(j % 2 == 0){
					cyl_v[j][2] = -edge_length / 2.0; // Bottom
				}else{
					cyl_v[j][2] = +edge_length / 2.0; // Top
				}
			}
			// Get a quaternion that rotates the cylinder to the edge orientation
			Eigen::Quaterniond quat;
			Eigen::Vector3d z(0.0, 0.0, 1.0);
			quat.setFromTwoVectors(z,edge_dir);
			// Add the cylinder's vertices
			for(size_t j = 0; j < cyl_v.size(); j++){
				// Rotate first, then displace center of cylinder to middle of edge
				vertex = quat * cyl_v[j] + shape_->v()[shape_->e()[i][0]] + 0.5 * edge_vec;
				// printf("v %f %f %f\n ",vertex[0],vertex[1],vertex[2]);
				swept_v.push_back( vertex );
			}
			// Add the cylinder's faces, keeping in mind that the indices will shift
			for(size_t j = 0; j < cyl_f.size(); j++){
				// The .array() allows us to add a scalar value to all Eigen::Vector3i's elements
				swept_f.push_back( cyl_f[j].array() + face_index_offset );
			}
		}
		// printf("Cylinders added: now have %lu vertices and %lu faces\n",swept_v.size(),swept_f.size());


		// For each face of non-swept shape, create a new one moved outward
		for(size_t i = 0; i < shape_->f().size(); i++){
			// Find face normal from cross product of two edges
			Eigen::Vector3d e1 = shape_->v()[ shape_->f()[i][1] ] - shape_->v()[ shape_->f()[i][0] ];
			Eigen::Vector3d e2 = shape_->v()[ shape_->f()[i][2] ] - shape_->v()[ shape_->f()[i][0] ];
			Eigen::Vector3d n = (e1.cross(e2)).normalized();
			// Create a new face with vertices at a distance of the swept radius in this normal direction
			if(shape_->f()[i].size() != 3){
				printf("Error in SphereSwept::save_to_obj: expected 3 integers for face, didnt get them.\n");
				exit(42);
			}
			for(int j = 0; j < shape_->f()[i].size(); j++){
				vertex = shape_->v()[ shape_->f()[i][j] ] + (n.array() * radius_).matrix();
				swept_v.push_back( vertex );
			}
			// Add the vertex indices of this face
			face << swept_v.size()-3, swept_v.size()-2, swept_v.size()-1;
			swept_f.push_back( face );
		}
		// printf("Flat faces added: now have %lu vertices and %lu faces\n",swept_v.size(),swept_f.size());



		// ============================= Time to save the object =======================================
		// Open a file to write to in output truncate mode (wipes preexisting content)
		std::string file_name = "SphereSwept_" + std::to_string(nr) + ".obj";
		std::ofstream shape_file;
	  shape_file.open(file_name, std::ios::out | std::ios::trunc);

		// Print vertices
		for(size_t i = 0; i < swept_v.size(); i++){
			shape_file << "v " << swept_v[i][0] << " " << swept_v[i][1] << " " << swept_v[i][2] << "\n";
		}

		// Calculate vertex normals
		// First make a list of the faces of each vertex
		std::vector<std::vector<int>> vf_index_map( swept_v.size() );
		for(size_t v_idx = 0; v_idx < swept_v.size(); v_idx++){
			// The index of every face that contains this vertex index gets added to the list
			for(size_t f_idx = 0; f_idx < swept_f.size(); f_idx++){
			const Eigen::Vector3i& f = swept_f[f_idx];
				for(int i = 0; i < f.size(); i++){
					unsigned int f_v_idx = f[i];
					if(f_v_idx == v_idx) vf_index_map[v_idx].push_back(f_idx);
				}
			}
		}
		// Then for every vertex calculate its normal as the sum of its adjacent face normals
		for(size_t v_idx = 0; v_idx < swept_v.size(); v_idx++){
			Eigen::Vector3d vertex_normal(0.0, 0.0, 0.0);
			for(int& f_idx : vf_index_map[v_idx]){
				// Normal of the face: cross product of the edges from its vertices
				const Eigen::Vector3i& f = swept_f[f_idx];
				Eigen::Vector3d fe1 = swept_v[f[1]] - swept_v[f[0]];
				Eigen::Vector3d fe2 = swept_v[f[2]] - swept_v[f[0]];
				vertex_normal += fe1.cross(fe2).normalized();
			}
			vertex_normal.normalize();
			shape_file << "vn " << vertex_normal[0] << " " << vertex_normal[1] << " " << vertex_normal[2] << "\n";
		}

		// Print faces
		for(size_t i = 0; i < swept_f.size(); i++){
			shape_file << "f ";
			for (int j = 0; j < swept_f[i].size(); j++){
				// shape_file << swept_f[i][j]+1 << " "; //+1 because most obj parsers start indices at 1, not 0
				shape_file << swept_f[i][j]+1 << "//" << swept_f[i][j]+1 << " ";
			}
			shape_file << "\n";
			// shape_file << "f " << faces_[i][0] << " " << faces_[i][1] << " " << faces_[i][2] << "\n";
		}

		shape_file.close();
		nr++;

		return " mesh " + file_name; // The return value is the specifier "mesh" plus the filename,
		// this tells the opengl code it needs to load a mesh from the file_name.
	}
}
