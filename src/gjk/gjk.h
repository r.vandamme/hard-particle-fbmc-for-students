#ifndef GJK_H
#define GJK_H

#include "transform.h"

namespace shape{
  class Convex;
}

namespace overlap{
  Eigen::Vector3d gjk_distance(const Transform&, const shape::Convex&, const Transform&, const shape::Convex&);
  bool gjk_boolean(const Transform&, const shape::Convex&, const Transform&, const shape::Convex&);
  bool gjk_raycast(const Transform&, const shape::Convex&, const Transform&, const shape::Convex&, const Eigen::Vector3d& ray_dir, double& t, Eigen::Vector3d& normal);
  //TODO: Think about returning closest point on A and distance vector.
  //Eigen::Vector3d gjk_closest_points(const Transform&, const shape::Convex&, const Transform&, const shape::Convex&, Eigen::Vector3d& pa, Eigen::Vector3d& pb);
}

#endif
