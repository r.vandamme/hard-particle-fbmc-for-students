#include "sim.h"


/* ---------==== Constructor (also main function) ====---------
 *
 */
Simulation::Simulation(InputParameters input_parameters){
  total_cycles = input_parameters.total_cycles;
  init_cycles = total_cycles; // TODO remove init_cycles
  n_particles = input_parameters.n_particles;

  // Load the particle shape from the obj file
  if(input_parameters.rounding_ratio == 0.0){ // Sharp polyhedron, no rounding
    shapes.push_back( new shape::Polyhedron(input_parameters.shape_file_name.c_str()) );
  }else if(input_parameters.rounding_ratio == 1.0){ // Max rounding, just a sphere
    shapes.push_back( new shape::Sphere(1.0) );
  }else{ // Something in between, a SphereSwept shape
    shapes.push_back(
      new shape::SphereSwept(
        new shape::Polyhedron(input_parameters.shape_file_name.c_str()),
        input_parameters.rounding_ratio
      )
    );
  }
  // printf("Volume of input shape: %lf\n",shapes[0]->volume());

  // Rescale particle size to ensure pressure range is appropriate
  for(size_t i = 0; i < shapes.size(); i++){
    shapes[i]->scale_by_factor( pow(1.0/shapes[i]->volume(),1.0/3.0) );
  }
  // printf("Volume of input shape: %lf\n",shapes[0]->volume());

  // Calculate the cutoff length for the particle interactions
  cutoff = 0.0;
  for(size_t i = 0; i < shapes.size(); i++){
    if( 2.0 * shapes[i]->out_radius() > cutoff ){
      cutoff = 2.0 * shapes[i]->out_radius();
    }
  }
  // printf("Cutoff radius for possible interactions: %lf\n",cutoff);

	// Lets RNG seed itself with processor clock cycles since startup
	std::cout << "Seeded RNG with seed " << RNG.seed_unique() << '\n';
	// RNG.seed(42);

    // Sets the starting and final pressures. x is later used to define the path from start to end.
	double starting_pressure = 10.0;
	double end_pressure = 1E5;
	double x = 0.0;
	pressure = starting_pressure;

  // Initialize a dilute system
  simple_initialization();

	// Try to eliminate any overlaps in the starting configuration, exit if fails
	eliminate_overlaps();

  // Do some scaling moves to equilibrate the volume to the pressure before deforming
  find_all_images();
	for(int i = 0; i < 10000; i++){
    if(n_particles > 1){
      Particle& p = particles[RNG.rani(1,n_particles-1)];
      MC_translate(p);
    }
		MC_scale();
	}
  lattice_reduction();
  find_all_images();
	save_snapshot("Begin");
  // box.print();

	// Randomly select an exponent for how fast the pressure is increased.
	double exponent = pow(3.0, RNG.rand(1.0, 1.3)); // Arbitrary: atm from ~ 3 to 4
	std::cout << "Exponent of compression route is " << exponent << '\n';

	// Monte Carlo loop
	std::cout << "Starting...\n";
	for(cycle = 0; cycle < total_cycles; cycle++){

		// Annealing step, increase the pressure
		if(cycle < init_cycles){
			// randomized exponent lets runs sample different compression routes
			pressure = starting_pressure + (end_pressure - starting_pressure)*pow(x/init_cycles,exponent);
			x += 1.0;
      // reset the MC move acceptance monitor variables
  		if(cycle % 1000 == 0){
  			// printf(" pressure now %lf\n",pressure);
  			try_translate = 0;  acc_translate = 0;
  			try_rotate    = 0;  acc_rotate    = 0;
  			try_scale     = 0;  acc_scale     = 0;
  			try_deform    = 0;  acc_deform    = 0;
  		}
    }

		// Perform as many MC steps as there are particles (convention)
		for(step = 0; step < particles.size(); step++){
			MC_Step();
		}
		// If still equilibrating,
		if(cycle < init_cycles){
			// update step sizes to keep acceptance ratios around 0.3/0.3/0.15/0.15
			if( frac_translate<0.2 && deltaTranslate>1E-4){ deltaTranslate*=0.99; }
			if( frac_translate>0.4 && deltaTranslate<1.0 ){ deltaTranslate*=1.01; }

			if( frac_rotate<0.2 && deltaRotate>1E-3      ){ deltaRotate*=0.99; }
			if( frac_rotate>0.4 && deltaRotate<0.5*M_PI  ){ deltaRotate*=1.01; }

			if(frac_scale<0.1 && deltaScale>1E-2 ){ deltaScale*=0.97; }
			if(frac_scale>0.2 && deltaScale<2.0  ){ deltaScale*=1.03; }

			if(frac_deform<0.1 && deltaDeform>1E-3){ deltaDeform*=0.99; }
			if(frac_deform>0.2 && deltaDeform<0.1 ){ deltaDeform*=1.01; }
		}

		// Sometimes print some text and save data
		if(cycle % (total_cycles/10) == 0){
      double packing_fraction = occupied_volume / box.volume();
			printf(" Cycle %lu of %lu : ",cycle,total_cycles);
			printf("Pressure: %lf   Acubicity: %lf   Packing fraction: %lf\n", pressure, box.acubicity(), packing_fraction);
			save_snapshot("Snapshot");
		}
	}

	// Try to make the unit cell more orthorhombic one last time
	lattice_reduction();
	if( Overlap_anywhere() ){
		std::cout << "Error: lattice reduction led to overlap!\n";
    box.print();
	}

  // Make some copies of the unit cell so we have enough neighbours to calculate bond order
  copy_cell(5);
  size_t n_images = 0;
  for(auto images : image_list){ n_images += images.size(); }
  std::vector<std::vector<double>> positions(n_particles+n_images,std::vector<double>(3));
  for(size_t i = 0; i < particles.size(); i++){
    positions[i][0] = particles[i].pos()[0];
    positions[i][1] = particles[i].pos()[1];
    positions[i][2] = particles[i].pos()[2];
  }
  std::vector<double> minpos(3, 0.0);
  std::vector<double> maxpos(3, 0.0);
  size_t counter = n_particles;
  for(size_t i = 0; i < image_list.size(); i++){
    for(size_t j = 0; j < image_list[i].size(); j++){
      for(size_t d = 0; d < 3; d++){
        positions[counter][d] = image_list[i][j][d];
        if(image_list[i][j][d] < minpos[d]){ minpos[d] = image_list[i][j][d]; }
        if(image_list[i][j][d] > maxpos[d]){ maxpos[d] = image_list[i][j][d]; }
      }
      counter++;
    }
  }
  for(size_t i = 0; i < n_particles+n_images; i++){
     for(size_t d = 0; d < 3; d++){
       positions[i][d] -= minpos[d];
     }
  }
  // Make a cubic box big enough to hold all copies of the unit cell
  std::vector<double> boxm(9, 0.0);
  for(size_t i = 0; i < 3; i++){
    boxm[4*i] = maxpos[i]-minpos[i]+1E-10; // offset to prevent edge cases
  }
  // Calculate the bond order parameters
  std::vector<std::vector<double>> q(n_particles, std::vector<double>(13));
  std::vector<std::vector<double>> w(n_particles, std::vector<double>(13));
  MSM::msm(positions, boxm, q, w);
  // Save the bond order parameters to a file
  std::ofstream file;
  file.open("q.txt", std::ios::out | std::ios::out);
  for(size_t i = 0; i < particles.size(); i++){
    for(size_t j = 0; j < q[0].size(); j++){file << q[i][j] << " ";}
    file << '\n';
  }
  file.close();
  file.open("w.txt", std::ios::out | std::ios::out);
  for(size_t i = 0; i < particles.size(); i++){
    for(size_t j = 0; j < w[0].size(); j++){file << w[i][j] << " ";}
    file << '\n';
  }
  file.close();

	// Before exiting, save observables
  double packing_fraction = occupied_volume / box.volume();
	save_number_to_file__write("PackingFraction",packing_fraction);

	// Print last line
	printf(" Cycle %lu of %lu : ",cycle, total_cycles);
	printf("Acubicity: %lf   Final packing fraction: %lf \n", box.acubicity(), packing_fraction);
	// Save a snapshot of the cell plus some images
	save_snapshot("Final");
	// And save the unit cell
	save_unit_cell("Unit_cell");
}


/* --------- Selects and performs an MC step. Also tracks acceptance ratios ---------
 * Choose a random Monte Carlo move from four options:
 * - Translate: Change a random particle's position
 * - Rotate:    Change a random particle's orientation
 * - Scale:     Change the size of the unit cell isotropically
 * - Deform:    Change the size of the unit cell anisotropically
 * For N==1 particles, we don't need translations so we redirect those to rotations.
 * After each step we calculate the new acceptance ratios.
 */
void Simulation::MC_Step(void){
	int N = particles.size();
	double random_number = RNG.rand(0.0,1.0);

	if(random_number < 0.35){
		// We don't need translation moves if we just have 1 particle per
		// unit cell, so redirect to rotation moves if that's the case
		if(N == 1){
			// Select a random particle
			Particle& p = particles[ RNG.rani(N-1) ];
			// MC rotation step
			acc_rotate += MC_rotate(p);
			try_rotate++;
			frac_rotate = (double) (acc_rotate/((double) try_rotate));
		}else{
			// Select a random particle
			Particle& p = particles[ RNG.rani(1,N-1) ];
			// MC translation step
			acc_translate += MC_translate(p);
			try_translate++;
			frac_translate = (double) (acc_translate/((double) try_translate));
		}
	}

	else if(random_number < 0.70){
		// Select a random particle
		Particle& p = particles[ RNG.rani(N-1) ];
		// MC rotation step
		acc_rotate += MC_rotate(p);
		try_rotate++;
		frac_rotate = (double) (acc_rotate/((double) try_rotate));
	}

	else if(random_number < 0.85){
		// MC scaling step
		acc_scale += MC_scale();
		try_scale++;
		frac_scale = (double) (acc_scale/((double) try_scale));
	}

	else if(random_number < 1.0){
		// MC deformation step
		acc_deform += MC_deform();
		try_deform++;
		frac_deform = (double) (acc_deform/((double) try_deform));
	}

}
/* --------- MC step that moves a particle ---------
 *
 */
bool Simulation::MC_translate(Particle& p){
	bool move_rejected = false;

	// Generate displacement vector
	Eigen::Vector3d random_vec       = RNG.randVec3d();
	Eigen::Vector3d displacement_vec = random_vec * deltaTranslate;

	// Move the particle, storing the old position
	Eigen::Vector3d old_pos( p.pos() );
	p.translate(displacement_vec);
	// Put the particle back into the box if step moves it outside.
	put_in_box(p);

	// Recreate particle p's images, storing the old list
	std::vector<Eigen::Vector3d> old_images( image_list[p.id()] );
	find_images_of(p);

	// Check for overlap for particle p with other particles and images
	if(move_rejected == false){
		if( Overlap(p) ){move_rejected = true;}
	}

	// Undo move if it is rejected
	if(move_rejected == true){
		// Move particle back
		p.set_pos(old_pos);
		image_list[p.id()] = old_images;
		return 0;  // move not accepted
	}else{
		return 1;
	}
}
/* --------- MC step that rotates a particle ---------
 *
 */
bool Simulation::MC_rotate(Particle& p){
	bool move_rejected = false;

	// Generate a random unit quaternion
	Eigen::Quaterniond rotation_quat = RNG.randomUnitQuaternion(deltaRotate);

	// Rotate the particle, saving the old orientation
	Eigen::Quaterniond old_rot( p.rot() );
	p.rotate(rotation_quat);

	// Check for overlap for particle p
	if(move_rejected == false){
		if( Overlap(p) ){move_rejected = true;}
	}

	// Undo move if it is rejected
	if(move_rejected == true){
		// Rotate particle back
		p.set_rot(old_rot);
		return 0;  // move not accepted
	}
	return 1;
}
/* --------- MC step that scales the simulation volume ---------
 *
 */
bool Simulation::MC_scale(void){
	unsigned int N = particles.size();
	bool move_rejected = false;

	// Copy box before move for if we need to reset
	Eigen::Matrix3d old_box_m = box.m();
	double Vold = box.volume();

	// Generate a change in volume and check whether it is accepted
	double Vnew = Vold * exp( RNG.rand(-0.5,+0.5)*deltaScale );
	if(Vnew < occupied_volume){return 0;} // Move rejected, packing fraction > 1
	double arg = exp( -pressure*(Vnew-Vold) + (N+1)*log(Vnew/Vold) );
	if(RNG.rand(0.0,1.0) > arg){return 0;} // Move rejected, nothing was changed yet so safely return

	// Make the volume change
	double factor = pow(Vnew/Vold, 1.0/3.0);
	// printf("vo vn arg factor %f %f %f %f\n",Vold,Vnew,arg,factor);
	Eigen::Matrix3d new_box_m = old_box_m.array() * factor;
	box.set(new_box_m);

	// Update particle positions
	for(size_t i = 0; i < N; i++){
		particles[i].set_pos( particles[i].pos().array() * factor );
	}

	// Recreate the list of particle images, storing the old one for if move is rejected
	std::vector<std::vector<Eigen::Vector3d>> old_image_list( image_list );
	find_all_images();

	// Check for overlap for all particles
	if(move_rejected == false){
		if( Overlap_anywhere() ){move_rejected = true;}
	}

	// Undo move if it is rejected
	if(move_rejected == true){
		// Reset box
		box.set(old_box_m);
		// Reset particles
		for(size_t i = 0; i < N; i++){
			particles[i].set_pos( particles[i].pos().array() * (1.0 / factor) );
		}
		// Reset images
		image_list = old_image_list;
		return 0;  // move not accepted
	}
	return 1; // move accepted
}
/* --------- MC step that deforms the simulation volume ---------
 *
 */
bool Simulation::MC_deform(void){
	unsigned int N = particles.size();

	// Copy box before move for if we need to reset
	Eigen::Matrix3d old_box_m = box.m();

  // Copy particles positions
	std::vector<Particle> old_particles = particles;

	// Generate a deformation
	double dv = RNG.rand(-0.5,0.5) * deltaDeform;
	Eigen::Matrix3d new_box_m = old_box_m;

	// Deform the box by changing a random element of its matrix
  int idx = RNG.rani(8);
  double Vold = box.volume();
  new_box_m(idx) += dv;
  box.set(new_box_m);
  double Vnew = box.volume();
  if(Vnew < occupied_volume){box.set(old_box_m); return 0;}  // Move rejected, packing fraction > 1
  double arg = exp( -pressure*(Vnew-Vold) + N*log(Vnew/Vold) );
  if(RNG.rand(0.0,1.0) > arg){box.set(old_box_m); return 0;} // Move rejected from Boltzmann weighting

	// divide particle coordinates by old box matrix, then multiply by new box matrix
  for(Particle& p : particles){
    p.affine_transform_pos(old_box_m, box.m());
  }

	// If box becomes too distorted from this deformation, apply lattice reduction
	if(box.acubicity() > 1.5){ // this number is fairly arbitrary
		lattice_reduction();
	}

	// Recreate the list of particle images, storing the old one for if move is rejected
	std::vector<std::vector<Eigen::Vector3d>> old_image_list( image_list );
	find_all_images();

  // Undo move if there are overlapping particles
	if( Overlap_anywhere() ){
		// Reset box, particles and image list
		box.set(old_box_m);
    particles = old_particles;
		image_list = old_image_list;
		return 0; // move not accepted
	}
	return 1; // move accepted
}


/* --------- Check whether a particle is overlapping with anything ---------
 * First checks for overlap with all other real particles in the box. Then checks
 * whether particle p is overlapping with its own periodic images. Then whether it
 * is overlapping with another particle's images.
 */
bool Simulation::Overlap(Particle& p){
	size_t N = particles.size();

	// Check for overlap with all particles except itself
	for(size_t j = 0; j < N; j++){
		if(j == p.id()){continue;} // Skip itself
		Particle& pj = particles[j];
		double dist_sq = (p.pos() - pj.pos()).squaredNorm();
		// First check outscribed radii whether they overlap, if they don't => no overlap
		if( dist_sq > pow( p.shape().out_radius() + pj.shape().out_radius(), 2) ){ continue; }
		// Then check inscribed radii: if they overlap => shapes overlap
		if( dist_sq < pow( p.shape().in_radius() + pj.shape().in_radius(), 2) ){return true;}
		// If outscribed radii overlap, check for overlap with GJK algorithm
		if( overlap::gjk_boolean(p.t(), p.shape(), pj.t(), pj.shape()) ){
			return true;
		}
	}

	// Check for overlap with its own images
	Particle sip(p); // self image particle (sip)
	for(Eigen::Vector3d& img_pos : image_list[ p.id() ]){
		double dist_sq = (p.pos() - img_pos).squaredNorm();
		if( dist_sq > pow( 2.0 * p.shape().out_radius(), 2) ){ continue; }
		if( dist_sq < pow( 2.0 * p.shape().in_radius(), 2) ){return true;}
		sip.set_pos(img_pos);
		if( overlap::gjk_boolean(p.t(), p.shape(), sip.t(), sip.shape()) ){
			return true;
		}
	}

	// Check for overlap with other particle's images
	for(size_t j = 0; j < N; j++){
		if(j == p.id()){ continue; } // Skip its own images, we already did this above
		Particle& pj = particles[j];
		Particle oip(pj); // other image particle (oip)
		// Check all images of pj for overlap with particle p
		for(Eigen::Vector3d& img_pos : image_list[j]){
			double dist_sq = (p.pos() - img_pos).squaredNorm();
			if( dist_sq > pow( p.shape().out_radius() + pj.shape().out_radius(), 2) ){ continue; }
			if( dist_sq < pow( p.shape().in_radius() + pj.shape().in_radius(), 2) ){return true;}
			oip.set_pos(img_pos);
			if( overlap::gjk_boolean(p.t(), p.shape(), oip.t(), oip.shape()) ){
				return true;
			}
		}
		// Also check reverse (whether images of p overlap with particle pj)
		Particle oip2(p); // other image particle (oip)
		for(Eigen::Vector3d& img_pos : image_list[ p.id() ]){
			double dist_sq = (pj.pos() - img_pos).squaredNorm();
			if( dist_sq > pow( p.shape().out_radius() + pj.shape().out_radius(), 2) ){ continue; }
			if( dist_sq < pow( p.shape().in_radius() + pj.shape().in_radius(), 2) ){return true;}
			oip2.set_pos(img_pos);
			if( overlap::gjk_boolean(pj.t(), pj.shape(), oip2.t(), oip2.shape()) ){
				return true;
			}
		}
	}

	// No particles or images overlap with p
	return false;
}
/* --------- Checks whether any particles are overlapping ---------
 * Checks all particles and their images for overlap.
 */
bool Simulation::Overlap_anywhere(void){
	for(Particle& p : particles){
		if( Overlap(p) ){ return true; }
	}
	return false;
}
/* --------- Finds pairs of particles that overlap ---------
 *
 */
std::vector<std::pair<size_t,size_t>> Simulation::find_overlaps(void){
	std::vector<std::pair<size_t,size_t>> overlap_pairs;
  // Checks all i<j pairs of particles.
  for(size_t i = 0; i < particles.size()-1; i++){
    Particle& pi = particles[i];
    for(size_t j = pi.id()+1; j < particles.size(); j++){
      Particle& pj = particles[j];
			if(pj.id() == pi.id()){continue;} // Skip itself
  		// First check outscribed radii whether they overlap, if they don't => no overlap
      double dist_sq = (pi.pos() - pj.pos()).squaredNorm();
  		if( dist_sq > pow( pi.shape().out_radius() + pj.shape().out_radius(), 2) ){ continue; }
  		// Then check inscribed radii: if they overlap => shapes overlap
  		if( dist_sq < pow( pi.shape().in_radius() + pj.shape().in_radius(), 2) ){
        overlap_pairs.push_back( std::make_pair(pi.id(),pj.id()) );
      }
			// std::cout << "Selected particles (indices): " << pi.id() << " " << pj.id() << '\n';
			if( overlap::gjk_boolean(pi.t(), pi.shape(), pj.t(), pj.shape()) ){
				overlap_pairs.push_back( std::make_pair(pi.id(),pj.id()) );
			}
		}
	}
	return overlap_pairs;
}


/* --------- Puts a particle back in the box ---------
 *
 */
void Simulation::put_in_box(Particle& p){
	Eigen::Vector3d PBC_vec;
	// Calculate relative position of particle in the simulation volume
	Eigen::Vector3d Relative_position = box.im() * p.pos();
	// While particle is outside of simulation box, add to the translation vector
	for(size_t d = 0; d < 3; d++){
		PBC_vec[d] = -floor(Relative_position[d]);
	}
	// Move the particle back into the box
	p.translate( box.m() * PBC_vec );
}
/* --------- Finds all periodic images of p and adds them to the list ---------
 *
 */
void Simulation::find_images_of(Particle& p){
	double image_cutoff = cutoff + 2.0 * box.out_radius();
	double image_cutoff_sq = image_cutoff * image_cutoff;
	static bool first = true;
	unsigned int pidx = p.id();

	static std::vector<Eigen::Vector3d> unit_cube_vertices(8);
	if(first){
		unit_cube_vertices[0] << -0.5, -0.5, -0.5;
		unit_cube_vertices[1] << -0.5, -0.5, +0.5;
		unit_cube_vertices[2] << -0.5, +0.5, -0.5;
		unit_cube_vertices[3] << -0.5, +0.5, +0.5;
		unit_cube_vertices[4] << +0.5, -0.5, -0.5;
		unit_cube_vertices[5] << +0.5, -0.5, +0.5;
		unit_cube_vertices[6] << +0.5, +0.5, -0.5;
		unit_cube_vertices[7] << +0.5, +0.5, +0.5;
		first = false;
		image_list.resize( particles.size() );
	}

	// Clear the old images of particle p
	image_list[pidx].clear();

	// Get the relative coords of a parallelepiped that contains all possible images
	std::vector<Eigen::Vector3d> image_ppiped_vert(8);
	for(int i = 0; i < 8; i++){
		image_ppiped_vert[i] = unit_cube_vertices[i].array() * cutoff;
		image_ppiped_vert[i] = box.im() * image_ppiped_vert[i];
	}

	// Find max number of images for each cardinal direction
	Eigen::Vector3d max_pos = image_ppiped_vert[0];
	for(int i = 1; i < 8; i++){
		if(image_ppiped_vert[i][0] > max_pos[0]){ max_pos[0] = image_ppiped_vert[i][0]; }
		if(image_ppiped_vert[i][1] > max_pos[1]){ max_pos[1] = image_ppiped_vert[i][1]; }
		if(image_ppiped_vert[i][2] > max_pos[2]){ max_pos[2] = image_ppiped_vert[i][2]; }
	}
	Eigen::Vector3i max_n;
	max_n[0] = (int) ceil(max_pos[0]) + 1;
	max_n[1] = (int) ceil(max_pos[1]) + 1;
	max_n[2] = (int) ceil(max_pos[2]) + 1;

	Eigen::Vector3d n = box.m().col(0) + box.m().col(1) + box.m().col(2);
	// From the max number of images, construct a list of image positions
	Eigen::Vector3d image_rel_pos, image_pos;
	for(int i = -max_n[0]; i <= max_n[0]; i++){
		for(int j = -max_n[1]; j <= max_n[1]; j++){
			for(int k = -max_n[2]; k <= max_n[2]; k++){
				if(i==0 && j==0 && k==0){ continue; } // Skip position of particle itself
        image_rel_pos[0] = double(i);
        image_rel_pos[1] = double(j);
        image_rel_pos[2] = double(k);
				image_pos = box.m() * image_rel_pos;
				// Point symmetry allows us to discard images not in the direction of n
				if( n.dot(image_pos) < 0.0 ){ continue; }
				// Discard image if it's too far away from the origin to matter
				if( image_pos.squaredNorm() > image_cutoff_sq ){ continue; }
				// Add image pos to array.
				// Add the particle position because up until now we were working in
				// a system where the particle was defined as being in the origin.
				image_list[pidx].push_back( p.pos() + image_pos );
			}
		}
	}
}
/* --------- Finds the periodic images of all particles and adds them to the list ---------
 *
 */
void Simulation::find_all_images(void){
	for(Particle& p : particles){
		find_images_of(p);
	}
}
/* --------- Reshape the simulation box to make it more orthorhombic ---------
 *
 */
void Simulation::lattice_reduction(void){
	// Store box matrix before adjustment
	Eigen::Matrix3d old_box_m( box.m() );
	double old_box_acubicity = box.acubicity();
	double old_box_area = box.surface_area();

	// Make some copies of the box to create variations of it later
  std::vector<SimulationBox> new_box_candidates(12);
  for(int iteration = 0; iteration < 10; iteration++){
    // Make some variations of the box that are hopefully more orthorhombic (no constraints)
    int c_idx = 0;
    for(int i = 0; i < 3; i++){
      for(int j = 0; j < 3; j++){
        if(i==j){ continue; }
        for(double sign = -1; sign <= +1; sign += 2){
          Eigen::Matrix3d temp_m = box.m();
          temp_m.col(i) += sign * box.m().col(j);
          new_box_candidates[c_idx].set(temp_m);
          c_idx++;
        }
      }
    }

		// Find the one with the smallest surface area
		double min_area = new_box_candidates[0].surface_area();
		int smallest_one = 0;
    for(size_t i = 1; i < new_box_candidates.size(); i++){
			double area = new_box_candidates[i].surface_area();
			if(area < min_area){
				min_area = area;
				smallest_one = i;
			}
		}

		// If this smallest area is larger than original, stop iterating.
		if(min_area > box.surface_area()){
      // Scale old positions into new relative space -> apply PBC -> scale into new real space
      for(size_t i = 1; i < particles.size(); i++){ // No need to scale first particle, it's @ 0,0,0
        Particle& p = particles[i];
        Eigen::Vector3d scaled_pos = box.im() * p.pos();
        for(size_t d = 0; d < 3; d++){ scaled_pos[d] -= floor(scaled_pos[d]); }
        p.set_pos(box.m() * scaled_pos);
      }
			return;
		}else{
			// Otherwise, set this more orthorhombic box as the new box
			box.set( new_box_candidates[smallest_one].m() );

		}
	}

	// Check for a particular error
	std::cout << " Warning: Lattice reduction did not converge!\n";
	if(box.acubicity() > old_box_acubicity){
		std::cout << "Error: lattice reduction resulted in more acubic box!" << '\n';
	}
	std::cout << "Old box: " << '\n';
	std::cout << old_box_m << '\n';
	std::cout << "Area, acub: " << old_box_area <<' '<< old_box_acubicity << '\n';
	for(int i = 0; i < 6; i++){
		std::cout << "Candidate " << i << ":\n";
		std::cout << "Area, acub: " << new_box_candidates[i].surface_area() <<' '<< new_box_candidates[i].acubicity()  << '\n';
		new_box_candidates[i].print();
	}
	std::cout << "New box: " << '\n';
	std::cout << box.m() << '\n';
	std::cout << "Area, acub: " << box.surface_area() <<' '<< box.acubicity()  << '\n';
	save_snapshot("error");
	exit(1);
}


/* --------- Create images for a nice visualization ---------
 * This function is mainly for a more intuitive visualization. Don't use it for simulation:
 * it creates way more images than needed and will only slow things down.
 */
void Simulation::visualize_images(void){
	// For all particles, generate images inside a cube
	double image_cutoff = 2.5 * pow(box.volume(),1.0/3.0);
	Eigen::Vector3d middle = box.middle();
	int n = 4; // Number of times to repeat the unit cell in each direction
	for(Particle& p : particles){
		// Clear the old images of particle p
		image_list[p.id()].clear();
		// Then make new images by copying the unit cell
		Eigen::Vector3d image_rel_pos, image_pos, real_pos;
		for(int i = -n; i <= n; i++){
			for(int j = -n; j <= n; j++){
				for(int k = -n; k <= n; k++){
					if(i==0 && j==0 && k==0){ continue; } // Skip position of particle itself
					image_rel_pos << (double) i, (double) j, (double) k;
					image_pos = box.m() * image_rel_pos;

					real_pos = p.pos() + image_pos;

					// Discard image if it's too far away from the middle of the box
					if( fabs(real_pos[0] - middle[0]) > image_cutoff ){ continue; }
					if( fabs(real_pos[1] - middle[1]) > image_cutoff ){ continue; }
					if( fabs(real_pos[2] - middle[2]) > image_cutoff ){ continue; }

					// Add image pos to array.
					image_list[p.id()].push_back(real_pos);
				}
			}
		}
	}
}
/* --------- Copies the unit cell a few times to create a larger crystal ---------
 * This function is mainly for a more intuitive visualization. Don't use it for simulation:
 * it creates way more images than needed and will only slow things down.
 */
void Simulation::copy_cell(size_t copies){
	if(copies > 1000){
		std::cout << "Error: >1000 copies of unit cell requested. Would likely crash. Exiting.\n";
		exit(42);
	}
	// For all particles, generate images inside a cube
	for(Particle& p : particles){
		// Clear the old images of particle p
		image_list[p.id()].clear();
		// Then make new images by copying the unit cell
		Eigen::Vector3d image_rel_pos, image_pos, real_pos;
		for(int i = -((int) copies); i <= ((int) copies); i++){
			for(int j = -((int) copies); j <= ((int) copies); j++){
				for(int k = -((int) copies); k <= ((int) copies); k++){
					if(i==0 && j==0 && k==0){ continue; } // Skip position of particle itself
					image_rel_pos << (double) i, (double) j, (double) k;
					image_pos = box.m() * image_rel_pos;
					real_pos = p.pos() + image_pos;
					// Add image pos to array.
					image_list[p.id()].push_back(real_pos);
				}
			}
		}
	}
}


/* --------- Initialize something simple ---------
 *
 */
void Simulation::simple_initialization(){
	// Set the box size
  box.set( 5.0 * cutoff * static_cast<double>(n_particles) );
  // box.print();

	// First particle should be in the origin:
  Transform t{ Eigen::Vector3d(0.0, 0.0, 0.0), RNG.randomUnitQuaternion(), 1.0 };
	// Make the particle, pass its index in the particle array & give it a shape
	particles.push_back( Particle(0, t, shapes[0]) );

	// The others can be randomly distributed
	for(size_t i = 1; i < n_particles; i++){
		Transform t{
    5.0 * cutoff * n_particles * Eigen::Vector3d(RNG.rand(0.0,1.0), RNG.rand(0.0,1.0), RNG.rand(0.0,1.0)),
      RNG.randomUnitQuaternion(),
      1.0
    };
		// Make a particle, pass its index in the particle array & let all particles have the same shape
		particles.push_back( Particle(i, t, shapes[0]) );
	}

	// Calculate the total volume occupied by particles
	occupied_volume = 0.0;
	for(Particle& p : particles){
		occupied_volume += p.shape().volume();
	}

	// Initialize the image list
	find_all_images();

	// save_snapshot("Loaded");

}
/* --------- Eliminate overlaps ---------
 *
 */
void Simulation::eliminate_overlaps(void){
 	size_t loops = 0;
 	// Find any overlapping pairs of particles
 	std::vector<std::pair<size_t,size_t>> overlap_pairs = find_overlaps();

 	// While there are overlapping pairs, move particles away from each other
 	while( !overlap_pairs.empty() ){
 		std::cout << "(Initial) configuration contains " << overlap_pairs.size() << " overlaps!\n";
 		std::cout << "Nudging particles to resolve overlaps..." << '\n';
 		// for(auto& pair : overlap_pairs){
 		// 	std::cout << pair.first << ' ' << pair.second << '\n';
 		// }
 		for(auto& pair : overlap_pairs){
 			Particle& pi = particles[pair.first];
 			Particle& pj = particles[pair.second];
 			// While the pair overlaps,
 			while( overlap::gjk_boolean(pi.t(), pi.shape(), pj.t(), pj.shape()) ){
 				// move i away from j a bit.
 				pi.translate( 0.1*cutoff*(box.nearest_image_vector(pj.pos(), pi.pos()).normalized()) );
 				put_in_box(pi);
 				// save_snapshot("Eliminating_overlap");
 			}
 		}
 		// Check whether moving these particles has created new overlaps
 		overlap_pairs = find_overlaps();
 		// If resolving overlaps takes too long, abort and give error
 		if(++loops > 10000){
 			std::cout << "Could not resolve overlaps. Exiting.\n";
 		}
 	}
 	std::cout << "Successfully resolved overlaps! Proceeding.\n";
 }


/* --------- Make a snapshot file of the system without images---------
 *
 */
void Simulation::save_unit_cell(const std::string name){

	// Save all shapes
	std::vector<std::string> shape_file_names;
	for(size_t i = 0; i < shapes.size(); i++){
		shape_file_names.push_back( shapes[i]->save_to_obj(i) );
	}

	// Make a list of which particles have which shape
	std::vector<int> particle_shape_id;
	for(size_t i = 0; i < particles.size(); i++){
		for(size_t j = 0; j < shapes.size(); j++){
			if(particles[i].shape_ptr() == shapes[j]){
				particle_shape_id.push_back(j);
			}
		}
	}

	// Open a file to write to
	std::ofstream snapshot_file;
	// Filename
	std::string snapshot_file_name = name + ".dat";
  snapshot_file.open(snapshot_file_name, std::ios::out | std::ios::trunc);


	// Save the number of particles
	snapshot_file << particles.size() << "\n";

	// Save the box. Do some Eigen stuff to ensure correct printing format
	Eigen::IOFormat MatrixOutputFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, " ", " ", "", "");
	snapshot_file << box.m().format(MatrixOutputFormat) << "\n";

	// Save the particle information
	for(size_t i = 0; i < particles.size(); i++){
		// Positions
		snapshot_file << particles[i].t().pos[0] << " " << particles[i].t().pos[1] << " " << particles[i].t().pos[2] << " ";
		// Orientations, in axis-angle representation
		Eigen::AngleAxisd AA(particles[i].t().rot);
		snapshot_file << AA.angle()*(360.0/(2.0*M_PI)) << " " << AA.axis()[0] << " " << AA.axis()[1] << " " << AA.axis()[2];
		// Number indicating the shape ID as defined at the end of this file
		// snapshot_file << " 0\n";
		snapshot_file << " " << particle_shape_id[i] << "\n";
	}

	// List of shape ID's and corresponding shape files
	if(shapes.size() != shape_file_names.size()){
		std::cout << "Shapes: " << shapes.size() << ". Shape files: " << shape_file_names.size() << "\n";
		exit(42);
	}
	for(size_t i = 0; i < shapes.size(); i++){
		snapshot_file << i << shape_file_names[i] << "\n";
	}

	// Close file stream and increment number of saved snapshots
	snapshot_file.close();
}
/* --------- Make a snapshot file of the system with images ---------
 *
 */
void Simulation::save_snapshot(const std::string name){
	static int nr = 0; // Number of the snapshot

	// Generate a cube of images
	visualize_images();

	// Save all shapes
	std::vector<std::string> shape_file_names;
	for(size_t i = 0; i < shapes.size(); i++){
		shape_file_names.push_back( shapes[i]->save_to_obj(i) );
	}

	// Make a list of which particles have which shape
	std::vector<int> particle_shape_id;
	for(size_t i = 0; i < particles.size(); i++){
		for(size_t j = 0; j < shapes.size(); j++){
			if(particles[i].shape_ptr() == shapes[j]){
				particle_shape_id.push_back(j);
			}
		}
	}

	// Open a file to write to
	std::ofstream snapshot_file;
	// Filename plus number with padded zeroes
	std::string padded_nr_string = std::string(4-std::to_string(nr).length(),'0')+std::to_string(nr);
	std::string snapshot_file_name = padded_nr_string + "_" + name + ".dat";
  snapshot_file.open(snapshot_file_name, std::ios::out | std::ios::trunc);

	// Save the number of particles plus the number of images
	int n_images = 0;
	for(auto element : image_list){ n_images += element.size(); }
	snapshot_file << particles.size() + n_images << "\n";

	// Save the box. Do some Eigen stuff to ensure correct printing format
	Eigen::IOFormat MatrixOutputFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, " ", " ", "", "");
	snapshot_file << box.m().format(MatrixOutputFormat) << "\n";

	// Save the particle information
	for(size_t i = 0; i < particles.size(); i++){
		// Positions
		snapshot_file << particles[i].t().pos[0] << " " << particles[i].t().pos[1] << " " << particles[i].t().pos[2] << " ";
		// Orientations, in axis-angle representation
		Eigen::AngleAxisd AA(particles[i].t().rot);
		snapshot_file << AA.angle()*(360.0/(2.0*M_PI)) << " " << AA.axis()[0] << " " << AA.axis()[1] << " " << AA.axis()[2];
		// Number indicating the shape ID as defined at the end of this file
		// snapshot_file << " 0\n";
		snapshot_file << " " << particle_shape_id[i] << "\n";
	}

	// Save the images' information
	for(size_t i = 0; i < image_list.size(); i++){
		for(size_t j = 0; j < image_list[i].size(); j++){
			// Positions
			snapshot_file << image_list[i][j][0] <<" "<< image_list[i][j][1] <<" "<< image_list[i][j][2] <<" ";
			// Orientations, in axis-angle representation
			Eigen::AngleAxisd AA(particles[i].t().rot);
			snapshot_file << AA.angle()*(360.0/(2.0*M_PI)) << " " << AA.axis()[0] << " " << AA.axis()[1] << " " << AA.axis()[2];
			// Number indicating the shape ID as defined at the end of this file
			// snapshot_file << " 0\n";
			snapshot_file << " " << particle_shape_id[i] << "\n";
		}
	}

	// List of shape ID's and corresponding shape files
	if(shapes.size() != shape_file_names.size()){
		std::cout << "Shapes: " << shapes.size() << ". Shape files: " << shape_file_names.size() << "\n";
		exit(42);
	}
	for(size_t i = 0; i < shapes.size(); i++){
		snapshot_file << i << shape_file_names[i] << "\n";
	}

	// Close file stream and increment number of saved snapshots
	snapshot_file.close();
	nr++;
}
/* --------- Saves one number to its own file --------
 *
 */
void Simulation::save_number_to_file__write(const std::string name, double number){
	// Open a file to write to
	std::ofstream file;
	std::string file_name = name + ".txt";
  file.open(file_name, std::ios::out | std::ios::trunc);
	// Write the number
	file << number << "\n";
	// Close file stream
	file.close();
}
/* --------- Appends one number to a file --------
 *
 */
void Simulation::save_number_to_file__append(const std::string name, double number){
	// Open a file to write to in append (std::ios::app) mode
	std::ofstream file;
	std::string file_name = name + ".txt";
  file.open(file_name, std::ios::out | std::ios::app);
	// Write the number
	file << number << "\n";
	// Close file stream
	file.close();
}
