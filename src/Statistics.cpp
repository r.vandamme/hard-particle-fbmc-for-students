#include "Statistics.h"


namespace statistics{

/*========================================== OBSERVABLE ==========================================*/

/* --------- Resets an observable to zero measurements ---------
 *
 */
void Observable::clear(void){
	samples_   = 0;
	mean_      = 0.0;
	mvariance_ = 0.0;
}
/* --------- Updates an observable after its current value is changed ---------
 * For expressions of incremental averages and variance, see
 * http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf
 * or just google it.
 */
void Observable::add_measurement(double value){
	double prev_mean = mean_;
	samples_++;
	mean_ += ( 1.0/ ((double) samples_) ) * (value - mean_);
	if(samples_ > 1){
		mvariance_ += (value - prev_mean) * (value - mean_);
	}
	// 	printf("    current value: %lf\n",value);
	// 	printf("       mean value: %lf\n",mean_);
	// 	printf("  prev_mean value: %lf\n",prev_mean);
	// 	printf("  mvariance value: %lf\n",mvariance_);
	// 	printf("     # of samples: %ld\n",samples_);
}
/* --------- Appends the current mean and variance to a file ---------
 *
 */
void Observable::save_append_to(const std::string file_name){
	static bool first = true;
	std::ofstream file;
	if(first){
		// Open a file to write to in truncate mode (make a new file!)
	  file.open(file_name, std::ios::out | std::ios::trunc);
		first = false;
	}else{
		// Open a file to write to in append mode
	  file.open(file_name, std::ios::out | std::ios::app);
	}
	// Save the data
	file << mean_ <<" "<< mvariance_/samples_ <<'\n';
	// Close file stream
	file.close();
}



/*========================================== HISTOGRAM ===========================================*/

// Constructor if we know the bin width
Histogram::Histogram(unsigned int n_bins, double binwidth){
	binwidth_ = binwidth;
	// Resize to the appropriate number of bins
	xvalues_.resize(n_bins);
	yvalues_.resize(n_bins);
	// Then initialize elements
	for(unsigned int i = 0; i < n_bins; i++){
		// Set the xvalues that are the centers of the bins
		double xvalue = ( ((double) i) + 0.5) * binwidth;
		xvalues_[i] = xvalue;
		// Set all yvalues to zero to make sure there's no weird leftover bits from memory
		yvalues_[i].clear();
	}
}

// Constructor if we know the domain of the histogram
Histogram::Histogram(unsigned int n_bins, double startx, double endx){
	binwidth_ = (endx-startx) / ((double) n_bins);
	// Resize to the appropriate number of bins
	xvalues_.resize(n_bins);
	yvalues_.resize(n_bins);
	// Then initialize elements
	for(unsigned int i = 0; i < n_bins; i++){
		double xvalue = ( ((double) i) + 0.5) * binwidth_;
		xvalues_[i] = xvalue;
		// Set all yvalues to zero to make sure there's no weird leftover bits from memory
		yvalues_[i].clear();
	}
}

// Add a measurement if we know which bin it falls into
void Histogram::add_measurement(unsigned int bin, double yvalue){
	if( bin > yvalues_.size() ){
		printf("Warning: bin exceeds size of histogram! Currently ignoring measurement!\n");
		return;
	}
	yvalues_[bin].add_measurement(yvalue);
}

// Add a measurement if we only have an xvalue instead
void Histogram::add_measurement(double xvalue, double yvalue){
	unsigned int bin = (int) floor(xvalue / binwidth_);
	add_measurement(bin, yvalue);
}

// Clears all observables in the histogram, but keeps xvalues
void Histogram::clear(void){
	// Reset all measured values
	for(unsigned int i = 0; i < yvalues_.size(); i++){
		yvalues_[i].clear();
	}
}

// Grows the histogram to a new size
void Histogram::grow_to(unsigned int new_n_bins){
	unsigned int old_n_bins = yvalues_.size();
	if(new_n_bins < old_n_bins){
		printf("Error: attempting to shrink histogram, could yield undefined behaviour.\n");
		exit(42);
	}
	// Resize to the appropriate number of bins
	xvalues_.resize(new_n_bins);
	yvalues_.resize(new_n_bins);
	// Then initialize elements
	for(unsigned int i = old_n_bins; i < new_n_bins; i++){
		// Set the xvalues that are the centers of the bins
		double xvalue = ( ((double) i) + 0.5) * binwidth_;
		xvalues_[i] = xvalue;
		// Set all yvalues to zero to make sure there's no weird leftover bits from memory
		yvalues_[i].clear();
	}
}






}
