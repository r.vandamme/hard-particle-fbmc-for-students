#ifndef COLLOID_FBMC_MAIN_H
#define COLLOID_FBMC_MAIN_H

#include "sim.h"

void process_command_line_input(int argc, char *argv[]);
int main(int argc, char *argv[]);

#endif // end header guard COLLOID_FBMC_MAIN_H
