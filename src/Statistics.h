#ifndef MC_CLASS_STATISTICS_H
#define MC_CLASS_STATISTICS_H

#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

/* Namespace that contains classes and functions to measure stuff and save it */
namespace statistics{


	class Observable{ // Class that describes a single observable scalar value e.g. density
	private:
		long int samples_ = 0;    // Number of times this value has been measured
		double mean_      = 0.0;  // Current mean of all measured values
		double mvariance_ = 0.0;  // Current variance times number of measurements
	public:
		// Constructors / destructors
		// Observable(void);
		// ~Observable(void);
		// Functions
		void clear(void);
		void add_measurement(double value);
		// Getters
		double variance(void)const{ return mvariance_ / samples_; }
		double stdev(void)const   { return sqrt(variance()); }
		double mean(void)const    { return mean_; }
		// Save to a file
		void save_append_to(const std::string filename);
	};



	class Histogram{ // Class that describes a histogram of scalar values e.g. density distribution
	private:
		double binwidth_;
		std::vector<double>     xvalues_;
		std::vector<Observable> yvalues_;
	public:
		// Constructors / destructors
		Histogram(unsigned int n_bins, double binwidth);
		Histogram(unsigned int n_bins, double startx, double endx);
		~Histogram(void);
		// Functions
		void clear(void);
		void clear_bin(unsigned int bin){ yvalues_[bin].clear(); }
		void add_measurement(unsigned int bin, double yvalue); // NOTE: should these be explicit?
		void add_measurement(double xvalue, double yvalue);
		void grow_to(unsigned int new_n_bins);
		// Setters
		// void set_size(size_t number_of_bins);
		// void save(char *filename);
	};




	// class ScalarField{ // Class that describes a spatial field of scalar values e.g. temperature field
	// };

	// class VectorField{ // Class that describes a spatial field of vector values e.g. flow field
	// };
}
























#endif
