#ifndef COLLOID_FBMC_SIM_H
#define COLLOID_FBMC_SIM_H


#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <memory>
// #include <eigen3/Eigen/Geometry>
#include "../external/eigen3/Eigen/Geometry"
#include "gjk/gjk.h"
#include "rnd.h"
#include "SimulationBox.h"
#include "Particle.h"
#include "Statistics.h"

#include "MinkowskiStructureMetrics.h"

// #include scene.h // Nick's partviewer3D for visualization

struct InputParameters {
	size_t total_cycles = 1E7;
	unsigned int n_particles = 1;
	double rounding_ratio = 0.0;
  std::string shape_file_name; // must be a .obj text file
};
extern InputParameters input_parameters;

class Simulation{
private:
	// Essential components: Particles, simulation volume and random number generator
	SimulationBox box;                    // Simulation volume
	std::vector<Particle> particles;      // Vector of particles
	tRnd RNG;                             // Using C++11's Marsenne Twister RNG
	// Component: in this simulation particles can have complex shapes. These are stored here:
	std::vector<shape::Convex*> shapes;
	// Lists of positions of periodic images of the particles
	std::vector<std::vector<Eigen::Vector3d>> image_list;

	// Maximum distance between two particles for which we will calculate the pair interaction
	double cutoff = 0.0;

  // Number of particles
	unsigned int n_particles = 1;
	// Thermodynamic state variables
	double pressure;
	// Volume occupied by particles
	double occupied_volume;

	// MC moves
	unsigned long int total_cycles, init_cycles, cycle=0, step=0;
	// Acceptance ratios, trackers and step sizes
	unsigned long int acc_translate=0, acc_rotate=0, acc_scale=0, acc_deform=0;  // counter for accepted MC moves
	unsigned long int try_translate=0, try_rotate=0, try_scale=0, try_deform=0;  // counter for attempted MC moves
	double frac_translate=0.3, frac_rotate=0.3, frac_scale=0.15, frac_deform=0.15; // ratio accepted/attempted MC moves
	double deltaTranslate=0.1, deltaRotate=0.1, deltaScale=0.1, deltaDeform=0.01; // Step sizes
	// Full 3D MC moves
	void MC_Step(void);
	bool MC_translate(Particle&);
	bool MC_rotate(Particle&);
	bool MC_scale(void);
	bool MC_deform(void);

	// Tries to find a more orthorhombic version of the simulation volume
	void lattice_reduction(void);


	// Calculates the packing fraction from the ratio of occupied and total volume
	double current_packing_fraction();

	// Overlap functions for particles with hard shapes: calls the GJK overlap algorithm
	bool Overlap(Particle&);
	bool Overlap_anywhere(void);
	std::vector<std::pair<size_t,size_t>> find_overlaps(void);

	// Places a particle back into the box if it is outside
	void put_in_box(Particle&);
	// Finds all periodic images of one particle and adds them to the image_list
	void find_images_of(Particle&);
	// Finds the periodic images of all particles and adds them to the image_list
	void find_all_images();
	// Makes a roughly cubic piece of crystal for visualization purposes
	void visualize_images(void);
	// Copies the unit cell a number of times to create a larger piece of crystal
	void copy_cell(size_t copies);

	// Initialization functions
  void simple_initialization();
	void initialization_from_file(double tetra_rounding, std::string init_file_name);
	void eliminate_overlaps(void);

	// Saves the current state of the particles, images and the box to a file
	void save_snapshot(const std::string file_name);
	// Saves the current state of just the particles and box to a file
	void save_unit_cell(const std::string file_name);
	// Saves numbers to either their own file or appends them to a file
	void save_number_to_file__write(const std::string file_name, double number);
	void save_number_to_file__append(const std::string file_name, double number);

public:
  Simulation(InputParameters);
	~Simulation(void){
		for(shape::Convex* s : shapes){
			delete s;
		}
  }

	// Getters
	SimulationBox& get_box(void){ return box; }
	std::vector<Particle>& get_particles(void){ return particles; }
	// CellList& get_cell_list(void){ return cell_list; }

	// Setters
};






#endif // end header guard COLLOID_FBMC_MAIN_H
