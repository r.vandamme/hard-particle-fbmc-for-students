#ifndef FBMC_CLASS_SIMBOX_H
#define FBMC_CLASS_SIMBOX_H

#include <iostream>
// #include <eigen3/Eigen/Geometry>
#include "../external/eigen3/Eigen/Geometry"

class SimulationBox{
private:
	Eigen::Matrix3d box_matrix_;
public:
	SimulationBox(void);
	SimulationBox(double x);
	SimulationBox(Eigen::Matrix3d matrix);
    ~SimulationBox(void){}

	// Setters
	void set(double x);
	void set(const Eigen::Matrix3d& matrix);

	// Getters
	const Eigen::Matrix3d& m(void)const{ return box_matrix_; }
	Eigen::Matrix3d im(void)const;
	Eigen::Matrix3d transpose(void)const;
	double in_radius(void)const;
	double out_radius(void)const;
	double volume(void)const;
	double acubicity(void)const;
	double surface_area(void)const;
	Eigen::Vector3d middle(void)const;

	// Periodic distance functions
	Eigen::Vector3d nearest_image_vector(const Eigen::Vector3d&, const Eigen::Vector3d&)const;
	double nearest_image_distancesq(const Eigen::Vector3d&, const Eigen::Vector3d&)const;
	double nearest_image_distance(const Eigen::Vector3d&, const Eigen::Vector3d&)const;
	Eigen::Vector3d nearest_image_pos(const Eigen::Vector3d&, const Eigen::Vector3d&)const;

  void print(void)const{
		std::cout << "box:\n";
		std::cout << box_matrix_ << '\n';
	}
};

// Getters
inline Eigen::Matrix3d SimulationBox::transpose(void)const{
	return box_matrix_.transpose();
}
inline Eigen::Matrix3d SimulationBox::im(void)const{
	return box_matrix_.inverse();
}

// Setters
inline void SimulationBox::set(const Eigen::Matrix3d& matrix){
	box_matrix_ = matrix;
}
inline void SimulationBox::set(double x){
	Eigen::Matrix3d matrix;
	matrix << x,0,0, 0,x,0, 0,0,x;
	set(matrix);
}

// Constructors
inline SimulationBox::SimulationBox(void){
	Eigen::Matrix3d matrix;
	matrix << 1,0,0, 0,1,0, 0,0,1;
	set(matrix);
}
inline SimulationBox::SimulationBox(double x){
	Eigen::Matrix3d matrix;
	matrix << x,0,0, 0,x,0, 0,0,x;
	set(matrix);
}
inline SimulationBox::SimulationBox(Eigen::Matrix3d matrix){
	set(matrix);
}


// Periodic distance functions

// Nearest image vector
inline Eigen::Vector3d SimulationBox::nearest_image_vector(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2)const{
	// Positions in relative coordinates
	Eigen::Vector3d rpos1 = box_matrix_.inverse() * pos1;
	Eigen::Vector3d rpos2 = box_matrix_.inverse() * pos2;

	// Relative non-periodic distance vector
	Eigen::Vector3d r12 = rpos2 - rpos1;

	// Absolute distance vector, to be updated in the next few lines
	Eigen::Vector3d nearvec = pos2 - pos1;

	// Check each cardinal direction for distance > box length, then correct nearvec accordingly
	for(size_t d = 0; d < 3; d++){
		if(     r12[d] > +0.5){nearvec -= box_matrix_.col(d);}
		else if(r12[d] < -0.5){nearvec += box_matrix_.col(d);}
	}

	// Return what is now the nearest image vector
	return nearvec;
}
// Nearest image distance squared
inline double SimulationBox::nearest_image_distancesq(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2)const{
	// Call nearest_image_vector and return its squared norm
	return nearest_image_vector(pos1, pos2).squaredNorm();
}
// Nearest image distance
inline double SimulationBox::nearest_image_distance(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2)const{
	// Call nearest_image_distancesq and return its square root
	return sqrt(nearest_image_distancesq(pos1, pos2));
}
// Returns the position of the image of 2 nearest to 1
inline Eigen::Vector3d SimulationBox::nearest_image_pos(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2)const{
	// Positions in relative coordinates
	Eigen::Vector3d rpos1 = box_matrix_.inverse() * pos1;
	Eigen::Vector3d rpos2 = box_matrix_.inverse() * pos2;

	// Relative non-periodic distance vector
	Eigen::Vector3d r12 = rpos2 - rpos1;

	// Position of image of 2 closest to 1, to be updated in the next few lines
	Eigen::Vector3d nearpos = pos2;

	// Check each cardinal direction for distance > box length, then correct nearpos accordingly
	for(size_t d = 0; d < 3; d++){
		if(     r12[d] > +0.5){nearpos -= box_matrix_.col(d);}
		else if(r12[d] < -0.5){nearpos += box_matrix_.col(d);}
	}

	// Return what is now the position of the nearest image
	return nearpos;
}

// Box geometric functions

// Returns how much this current box shape resembles a cube
inline double SimulationBox::acubicity(void)const{
	double factor = (box_matrix_.col(0).norm() + box_matrix_.col(1).norm() + box_matrix_.col(2).norm()) * this->surface_area();
	// average vec length times surface area divided by volume, normalized so for a cube it's 1.0
	return (1.0/18.0) * factor / this->volume();
}
// Returns how large the surface area of the box is
inline double SimulationBox::surface_area(void)const{
	Eigen::Vector3d cross01 = box_matrix_.col(0).cross(box_matrix_.col(1));
	Eigen::Vector3d cross12 = box_matrix_.col(1).cross(box_matrix_.col(2));
	Eigen::Vector3d cross20 = box_matrix_.col(2).cross(box_matrix_.col(0));
	return 2.0 * (cross01.norm() + cross12.norm() + cross20.norm());
}
// Returns the volume of the current box
inline double SimulationBox::volume(void)const{
	return fabs(box_matrix_.determinant());
}
// Returns the radius of the inscribed sphere of the parallelepiped spanning the box
inline double SimulationBox::in_radius(void)const{
	double volume = this->volume();
	// Calculate the radius of the inscribed sphere.
	// It's the distance from center of box to the nearest parallelepiped face.
	double d0 = volume / (box_matrix_.col(1).cross( box_matrix_.col(2) )).norm();
	double d1 = volume / (box_matrix_.col(2).cross( box_matrix_.col(0) )).norm();
	double d2 = volume / (box_matrix_.col(0).cross( box_matrix_.col(1) )).norm();
	double in_radius = 0.5 * d0;
	if(0.5 * d1 < in_radius){ in_radius = 0.5 * d1; }
	if(0.5 * d2 < in_radius){ in_radius = 0.5 * d2; }
	return in_radius;
}
// Returns the radius of the circumscribed sphere of the parallelepiped spanning the box
inline double SimulationBox::out_radius(void)const{
	// Get middle of box
	// Eigen::Vector3d middle = 0.5 * (box_matrix_.col(0) + box_matrix_.col(1) + box_matrix_.col(2) );
	// // Get four vertices of box parallelepiped with origin in its middle
	// std::vector<Eigen::Vector3d> v(4);
	// v[0] = -middle;
	// v[1] = box_matrix_.col(0) - middle;
	// v[2] = box_matrix_.col(1) - middle;
	// v[3] = box_matrix_.col(2) - middle;
	// // The largest norm of these four vertices is the circumradius
	// double out_rad_sq = v[0].squaredNorm();
	// if( v[1].squaredNorm() > out_rad_sq ){ out_rad_sq = v[1].squaredNorm(); }
	// if( v[2].squaredNorm() > out_rad_sq ){ out_rad_sq = v[2].squaredNorm(); }
	// if( v[3].squaredNorm() > out_rad_sq ){ out_rad_sq = v[3].squaredNorm(); }
	// return sqrt(out_rad_sq);

	// Half of the norm of the sum of the absolute value of the box vectors is the circumradius
	return 0.5 * (
		abs(box_matrix_.col(0).array()) +
		abs(box_matrix_.col(1).array()) +
		abs(box_matrix_.col(2).array()) ).matrix().norm();

}
// Returns a vector from 0,0,0 to the middle of the box
inline Eigen::Vector3d SimulationBox::middle(void)const{
	return 0.5 * (box_matrix_.col(0) + box_matrix_.col(1) + box_matrix_.col(2) );
}

#endif
