#ifndef FBMC_PARTICLE_H
#define FBMC_PARTICLE_H

// #include <eigen3/Eigen/Geometry>
#include "../external/eigen3/Eigen/Geometry"
#include "gjk/shape/convex.h"
#include "gjk/shape/shapes.h"
#include "gjk/transform.h"
#include <memory>

class Particle{
private:
	// Properties
	unsigned int index_; // index_ of this particle in the list of particles
	Transform t_; // This structure contains the particle's current position, orientation and size
	shape::Convex *shape_; // Pointer to a shape that is defined in the sim:: scope.
public:
	// Constructors
    Particle(unsigned int id, Transform t, shape::Convex *shape_ptr) :
		index_(id), t_(t), shape_(shape_ptr)
	{}
	// Copy / move constructors
	Particle(const Particle&);
	// Destructor
    ~Particle(void){}

	// Functions to move, rotate, and transform
	void translate(const Eigen::Vector3d& displacement_vec);
	void rotate(const Eigen::Quaterniond& rotation_quat);
	void affine_transform_pos(const Eigen::Matrix3d& old_box_m, const Eigen::Matrix3d& new_box_m);

	// Set properties
	void set_pos(const Eigen::Vector3d& pos)   { t_.pos = pos; }
	void set_rot(const Eigen::Quaterniond& rot){ t_.rot = rot; }
	// Get properties
    unsigned int id(void)const         { return index_;  }
	const Transform& t(void)const            { return t_;     }
	const Eigen::Vector3d& pos(void)const    { return t_.pos; }
	const Eigen::Quaterniond& rot(void)const { return t_.rot; }
	const shape::Convex& shape(void)const{
		if(shape_){	return *shape_; // Return a reference to the shape
		}else{ // Uh oh, *shape_ doesn't exist!
			printf("Error: requested shape that was never created! Exiting.\n");
			exit(42);
		}
	}
	const shape::Convex* shape_ptr(void)const{return shape_;}
    void print_transform(void)const{t_.print();}
};

// Copy constructor that clones the position, orientation and shape from another particle
inline Particle::Particle(const Particle& other) : index_(other.index_), t_(other.t_) {
	shape_ = other.shape_;
}


// Function that displaces the particle
inline void Particle::translate(const Eigen::Vector3d& displacement_vec){
	// Just the position
	t_.pos += displacement_vec;

	// Later on we could store things like shape vertex positions to speed up overlap checks
	// That would require updating them whenever the particle is moved, so we do that here
}

// Function that rotates the particle
inline void Particle::rotate(const Eigen::Quaterniond& rotation_quat){
	// Just the orientation
	t_.rot *= rotation_quat;

	// Later on we could store things like shape vertex positions to speed up overlap checks
	// That would require updating them whenever the particle is rotated, so we do that here
}

// Function that applies an affine transformation from one box matrix to another to the position
inline void Particle::affine_transform_pos(const Eigen::Matrix3d& old_box_m, const Eigen::Matrix3d& new_box_m){
	Eigen::Vector3d scaled_pos = old_box_m.inverse() * t_.pos;
	// printf("%f %f %f\n",scaled_pos[0],scaled_pos[1],scaled_pos[2]);
	t_.pos = new_box_m * scaled_pos;
}


#endif
